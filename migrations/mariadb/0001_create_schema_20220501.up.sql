-- highkick_schedulers definition

CREATE TABLE `highkick_schedulers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `job_sid` varchar(255) NOT NULL DEFAULT '',
  `job_input` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT json_object() CHECK (json_valid(`job_input`)),
  `run_every_seconds` int(11) unsigned NOT NULL DEFAULT 0,
  `stopped` tinyint(1) NOT NULL DEFAULT 0,
  `last_run_at` datetime DEFAULT NULL,
  `last_error` varchar(255) NOT NULL DEFAULT '',
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `scheduler_type` enum('timer','exact_time') NOT NULL DEFAULT 'timer',
  `exact_times` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- highkick_workers definition

CREATE TABLE `highkick_workers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sid` varchar(255) NOT NULL DEFAULT '',
  `process_sid` varchar(255) NOT NULL DEFAULT '',
  `stopped` tinyint(1) NOT NULL DEFAULT 0,
  `running_jobs_count` int(11) NOT NULL DEFAULT 0,
  `healthchecked_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;


-- job_logs definition

CREATE TABLE `job_logs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `job_path` varchar(255) NOT NULL DEFAULT '',
  `content` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `job_logs_job_id_idx` (`job_id`),
  KEY `job_logs_job_path_idx` (`job_path`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- jobs definition

CREATE TABLE `jobs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sid` varchar(255) NOT NULL DEFAULT '',
  `path` varchar(255) NOT NULL DEFAULT '',
  `input` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`input`)),
  `output` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`output`)),
  `status` enum('initial','processing','failed','completed','scheduled') NOT NULL DEFAULT 'initial',
  `retries_left` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `cron` varchar(255) DEFAULT NULL,
  `logs_count` int(11) NOT NULL DEFAULT 0,
  `started_at` datetime DEFAULT NULL,
  `finished_at` datetime DEFAULT NULL,
  `worker_id` int(11) NOT NULL DEFAULT 0,
  `full_path` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '[]' CHECK (json_valid(`full_path`)),
  `full_path1` int(11) GENERATED ALWAYS AS (coalesce(json_unquote(json_extract(`full_path`,'$[0]')),0)) VIRTUAL,
  `full_path2` int(11) GENERATED ALWAYS AS (coalesce(json_unquote(json_extract(`full_path`,'$[1]')),0)) VIRTUAL,
  `full_path3` int(11) GENERATED ALWAYS AS (coalesce(json_unquote(json_extract(`full_path`,'$[2]')),0)) VIRTUAL,
  `full_path4` int(11) GENERATED ALWAYS AS (coalesce(json_unquote(json_extract(`full_path`,'$[3]')),0)) VIRTUAL,
  `full_path5` int(11) GENERATED ALWAYS AS (coalesce(json_unquote(json_extract(`full_path`,'$[4]')),0)) VIRTUAL,
  `full_path6` int(11) GENERATED ALWAYS AS (coalesce(json_unquote(json_extract(`full_path`,'$[5]')),0)) VIRTUAL,
  `full_path7` int(11) GENERATED ALWAYS AS (coalesce(json_unquote(json_extract(`full_path`,'$[6]')),0)) VIRTUAL,
  `full_path8` int(11) GENERATED ALWAYS AS (coalesce(json_unquote(json_extract(`full_path`,'$[7]')),0)) VIRTUAL,
  `full_path9` int(11) GENERATED ALWAYS AS (coalesce(json_unquote(json_extract(`full_path`,'$[8]')),0)) VIRTUAL,
  `full_path10` int(11) GENERATED ALWAYS AS (coalesce(json_unquote(json_extract(`full_path`,'$[9]')),0)) VIRTUAL,
  `full_path11` int(11) GENERATED ALWAYS AS (coalesce(json_unquote(json_extract(`full_path`,'$[10]')),0)) VIRTUAL,
  `full_path12` int(11) GENERATED ALWAYS AS (coalesce(json_unquote(json_extract(`full_path`,'$[11]')),0)) VIRTUAL,
  `full_path13` int(11) GENERATED ALWAYS AS (coalesce(json_unquote(json_extract(`full_path`,'$[12]')),0)) VIRTUAL,
  `full_path14` int(11) GENERATED ALWAYS AS (coalesce(json_unquote(json_extract(`full_path`,'$[13]')),0)) VIRTUAL,
  `full_path15` int(11) GENERATED ALWAYS AS (coalesce(json_unquote(json_extract(`full_path`,'$[14]')),0)) VIRTUAL,
  PRIMARY KEY (`id`),
  KEY `jobs_path_idx` (`path`),
  KEY `jobs_status_idx` (`status`),
  KEY `jobs_sid_idx` (`sid`),
  KEY `jobs_fullpath1_idx` (`full_path1`),
  KEY `jobs_fullpath2_idx` (`full_path2`),
  KEY `jobs_fullpath3_idx` (`full_path3`),
  KEY `jobs_fullpath4_idx` (`full_path4`),
  KEY `jobs_fullpath5_idx` (`full_path5`),
  KEY `jobs_fullpath6_idx` (`full_path6`),
  KEY `jobs_fullpath7_idx` (`full_path7`),
  KEY `jobs_fullpath8_idx` (`full_path8`),
  KEY `jobs_fullpath9_idx` (`full_path9`),
  KEY `jobs_fullpath10_idx` (`full_path10`),
  KEY `jobs_fullpath11_idx` (`full_path11`),
  KEY `jobs_fullpath12_idx` (`full_path12`),
  KEY `jobs_fullpath13_idx` (`full_path13`),
  KEY `jobs_fullpath14_idx` (`full_path14`),
  KEY `jobs_fullpath15_idx` (`full_path15`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;