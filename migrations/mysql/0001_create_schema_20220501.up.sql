-- saytube.highkick_schedulers definition

CREATE TABLE `highkick_schedulers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `job_sid` varchar(255) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `job_input` json NOT NULL DEFAULT (json_object()),
  `run_every_seconds` int(11) unsigned NOT NULL DEFAULT '0',
  `stopped` tinyint(1) NOT NULL DEFAULT '0',
  `last_run_at` datetime DEFAULT NULL,
  `last_error` varchar(255) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `scheduler_type` enum('timer','exact_time') COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'timer',
  `exact_times` varchar(255) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


-- saytube.highkick_workers definition

CREATE TABLE `highkick_workers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sid` varchar(255) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `process_sid` varchar(255) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `stopped` tinyint(1) NOT NULL DEFAULT '0',
  `running_jobs_count` int(11) NOT NULL DEFAULT '0',
  `healthchecked_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


-- saytube.job_logs definition

CREATE TABLE `job_logs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `job_path` varchar(255) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `content` text COLLATE utf8mb4_general_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `job_logs_job_id_idx` (`job_id`),
  KEY `job_logs_job_path_idx` (`job_path`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


-- saytube.jobs definition

CREATE TABLE `jobs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sid` varchar(255) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `path` varchar(255) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `input` json DEFAULT NULL,
  `output` json DEFAULT NULL,
  `status` enum('scheduled','initial','processing','failed','completed') COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'initial',
  `retries_left` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cron` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `logs_count` int(11) NOT NULL DEFAULT '0',
  `started_at` datetime DEFAULT NULL,
  `finished_at` datetime DEFAULT NULL,
  `worker_id` int(11) NOT NULL DEFAULT '0',
  `full_path` json NOT NULL DEFAULT (_utf8mb4'[]'),
  `full_path1` int(11) GENERATED ALWAYS AS (coalesce(json_unquote(json_extract(`full_path`,_utf8mb4'$[0]')),0)) STORED NOT NULL,
  `full_path2` int(11) GENERATED ALWAYS AS (coalesce(json_unquote(json_extract(`full_path`,_utf8mb4'$[1]')),0)) STORED NOT NULL,
  `full_path3` int(11) GENERATED ALWAYS AS (coalesce(json_unquote(json_extract(`full_path`,_utf8mb4'$[2]')),0)) STORED NOT NULL,
  `full_path4` int(11) GENERATED ALWAYS AS (coalesce(json_unquote(json_extract(`full_path`,_utf8mb4'$[3]')),0)) STORED NOT NULL,
  `full_path5` int(11) GENERATED ALWAYS AS (coalesce(json_unquote(json_extract(`full_path`,_utf8mb4'$[4]')),0)) STORED NOT NULL,
  `full_path6` int(11) GENERATED ALWAYS AS (coalesce(json_unquote(json_extract(`full_path`,_utf8mb4'$[5]')),0)) STORED NOT NULL,
  `full_path7` int(11) GENERATED ALWAYS AS (coalesce(json_unquote(json_extract(`full_path`,_utf8mb4'$[6]')),0)) STORED NOT NULL,
  `full_path8` int(11) GENERATED ALWAYS AS (coalesce(json_unquote(json_extract(`full_path`,_utf8mb4'$[7]')),0)) STORED NOT NULL,
  `full_path9` int(11) GENERATED ALWAYS AS (coalesce(json_unquote(json_extract(`full_path`,_utf8mb4'$[8]')),0)) STORED NOT NULL,
  `full_path10` int(11) GENERATED ALWAYS AS (coalesce(json_unquote(json_extract(`full_path`,_utf8mb4'$[9]')),0)) STORED NOT NULL,
  `full_path11` int(11) GENERATED ALWAYS AS (coalesce(json_unquote(json_extract(`full_path`,_utf8mb4'$[10]')),0)) STORED NOT NULL,
  `full_path12` int(11) GENERATED ALWAYS AS (coalesce(json_unquote(json_extract(`full_path`,_utf8mb4'$[11]')),0)) STORED NOT NULL,
  `full_path13` int(11) GENERATED ALWAYS AS (coalesce(json_unquote(json_extract(`full_path`,_utf8mb4'$[12]')),0)) STORED NOT NULL,
  `full_path14` int(11) GENERATED ALWAYS AS (coalesce(json_unquote(json_extract(`full_path`,_utf8mb4'$[13]')),0)) STORED NOT NULL,
  `full_path15` int(11) GENERATED ALWAYS AS (coalesce(json_unquote(json_extract(`full_path`,_utf8mb4'$[14]')),0)) STORED NOT NULL,
  PRIMARY KEY (`id`),
  KEY `jobs_path_idx` (`path`),
  KEY `jobs_status_idx` (`status`),
  KEY `jobs_sid_idx` (`sid`),
  KEY `jobs_fullpath1_idx` (`full_path1`),
  KEY `jobs_fullpath2_idx` (`full_path2`),
  KEY `jobs_fullpath3_idx` (`full_path3`),
  KEY `jobs_fullpath4_idx` (`full_path4`),
  KEY `jobs_fullpath5_idx` (`full_path5`),
  KEY `jobs_fullpath6_idx` (`full_path6`),
  KEY `jobs_fullpath7_idx` (`full_path7`),
  KEY `jobs_fullpath8_idx` (`full_path8`),
  KEY `jobs_fullpath9_idx` (`full_path9`),
  KEY `jobs_fullpath10_idx` (`full_path10`),
  KEY `jobs_fullpath11_idx` (`full_path11`),
  KEY `jobs_fullpath12_idx` (`full_path12`),
  KEY `jobs_fullpath13_idx` (`full_path13`),
  KEY `jobs_fullpath14_idx` (`full_path14`),
  KEY `jobs_fullpath15_idx` (`full_path15`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;