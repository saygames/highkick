ALTER TABLE highkick_schedulers ADD COLUMN `cron` VARCHAR(64) NOT NULL DEFAULT "";
ALTER TABLE highkick_schedulers MODIFY COLUMN scheduler_type enum('timer','exact_time','cron');