import React from 'react'

import { store as ReduxStore } from '../../redux/store'

import Toast, { ToastType } from '../../models/toast'
import * as ToastActions from '../../redux/toasts/actions'

import JobToast from '../highkick/job_toast'

type Props = {
  toast: Toast
}

class ToastItem extends React.Component<Props> {
  render() {
    const { toast } = this.props

    const remove = (toast: Toast) => {
      // Yep. Dispatch action directly to the store
      ReduxStore.dispatch(new ToastActions.RemoveToast(toast))
    }

    return (
      <div className="toast show overflow-auto m-0" role="alert" style={{opacity: 1, maxHeight: 200}}>
        <div className="toast-header" style={{position: 'relative'}}>
          <span className="mr-2">
            { (toast.type === ToastType.Notice) && <span></span> }
            { (toast.type === ToastType.Important) && <span>!!!</span> }
          </span>
          
          <div className="mr-auto"
            dangerouslySetInnerHTML={{__html: toast.title}}
          />
          
          <button
            type="button"
            className="ml-2 mb-1 close"
            onClick={() => remove(this.props.toast)}
            style={{ position: 'absolute', right: '10px' }}
          >
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        {/* if a string */}
        { (!!toast.content) && (
          <div 
            className="toast-body"
            dangerouslySetInnerHTML={{__html: toast.content!}}
          />
        ) }
        {/* if a component */}
        { (toast.contentComponentName === 'JobToast') && (
          <div className="toast-body">
            {React.createElement(JobToast, toast.contentComponentProps)}
          </div>
        ) }
      </div>)
  }
}

export default ToastItem