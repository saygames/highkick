module bitbucket.org/saygames/highkick

go 1.21

toolchain go1.21.1

require (
	bitbucket.org/saygames/repository v1.0.37
	github.com/AlekSi/pointer v1.1.0
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.5.0
	github.com/go-sql-driver/mysql v1.7.1
	github.com/google/uuid v1.6.0
	github.com/gorilla/websocket v1.4.0
	github.com/invopop/jsonschema v0.6.0
	github.com/markbates/pkger v0.17.1
	github.com/pkg/errors v0.9.1
	github.com/qw4n7y/gopubsub v0.0.0-20190731135927-57ca0b200c55
	github.com/tidwall/gjson v1.7.5
	gopkg.in/reform.v1 v1.5.1
)

require (
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/hashicorp/go-multierror v1.1.1 // indirect
	github.com/robfig/cron/v3 v3.0.1 // indirect
	go.uber.org/atomic v1.7.0 // indirect
	golang.org/x/crypto v0.8.0 // indirect
	golang.org/x/sync v0.2.0 // indirect
	google.golang.org/protobuf v1.30.0 // indirect
)

require (
	github.com/gin-contrib/sse v0.1.0 // indirect
	github.com/go-co-op/gocron v1.18.1
	github.com/go-playground/locales v0.12.1 // indirect
	github.com/go-playground/universal-translator v0.16.0 // indirect
	github.com/gobuffalo/here v0.6.0 // indirect
	github.com/golang-migrate/migrate/v4 v4.16.2
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/iancoleman/orderedmap v0.0.0-20190318233801-ac98e3ecb4b0 // indirect
	github.com/json-iterator/go v1.1.7 // indirect
	github.com/leodido/go-urn v1.1.0 // indirect
	github.com/mattn/go-isatty v0.0.16 // indirect
	github.com/modern-go/concurrent v0.0.0-20180228061459-e0a39a4cb421 // indirect
	github.com/modern-go/reflect2 v0.0.0-20180701023420-4b7aa43c6742 // indirect
	github.com/tidwall/match v1.0.3 // indirect
	github.com/tidwall/pretty v1.1.0 // indirect
	github.com/ugorji/go/codec v1.1.7 // indirect
	golang.org/x/sys v0.23.0 // indirect
	gopkg.in/go-playground/validator.v9 v9.29.1 // indirect
	gopkg.in/yaml.v2 v2.2.7 // indirect
)
