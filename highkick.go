package highkick

import (
	"github.com/markbates/pkger"

	"bitbucket.org/saygames/highkick/internal/database"
	"bitbucket.org/saygames/highkick/internal/domain"
	"bitbucket.org/saygames/highkick/internal/lib"
	"bitbucket.org/saygames/highkick/internal/server"
	"bitbucket.org/saygames/highkick/internal/usecases"
	"bitbucket.org/saygames/highkick/internal/workers"

	_ "bitbucket.org/saygames/highkick/internal/usecases"
)

func init() {
	// Include is a no-op that directs the pkger tool to include the desired file or folder.
	pkger.Include("/client/build")
	pkger.Include("/migrations")
}

// Domain
type JobMeta = domain.JobMeta
type Job = domain.Job
type Input = domain.JSONDictionary
type JobsToHandle = domain.JobsToHandle
type Worker = workers.Worker
type JSONDictionary = domain.JSONDictionary
type JobStatus = domain.JobStatus
type GetJobIDsParams = usecases.GetJobIDsParams

var JobStatuses = domain.JobStatuses
var NewJob = domain.NewJob
var NewJob2 = domain.NewJob2

var Register = usecases.Register
var GetOutput = usecases.GetOutput
var GetOutputByKey = usecases.GetOutputByKey
var SetOutput = usecases.SetOutput
var SetOutputRaw = usecases.SetOutputRaw
var GetOutputRaw = usecases.GetOutputRaw
var GetJSONSchemaFor = lib.GetJSONSchemaFor
var GetJobIDs = usecases.GetJobIDs
var GetParent = usecases.GetParent

// Database
type DatabaseOptions = database.DatabaseOptions

var DatabaseEngines = database.DatabaseEngines
var SetupDatabase = database.Setup

// Server
type ServerParams = server.ServerParams

var RunServer = server.RunServer

// Execution
var RunSync = usecases.RunSync
var RunAsync = usecases.RunAsync
var RetrySync = usecases.RetrySync
var RetryAsync = usecases.RetryAsync
var AttemptRetrySync = usecases.AttemptRetrySync   // retry with retries left check
var AttemptRetryAsync = usecases.AttemptRetryAsync // retry with retries left check

// Workers
type WorkerParams = usecases.WorkerParams

var RegisterAndRunWorker = usecases.RegisterAndRunWorker
var RunWorkerMonitor = usecases.RunWorkerMonitor

// Scheduling
type RunSchedulersParams = usecases.RunSchedulersParams

var RunSchedulers = usecases.RunSchedulers

// Locking
var Lock = usecases.Lock
var Unlock = usecases.Unlock

// Logging
var Log = usecases.Log

// Brodcasting

var SetupJobUpdateBroadcasting = usecases.SetupJobUpdateBroadcasting

// PubSub
type PubSubMessage = domain.PubSubMessage

var JobsPubSub = usecases.JobsPubSub
