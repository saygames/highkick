import React from 'react'

export default function Loader(props: {}) {
  return (
    <div className='w-100 h-100 d-flex'>
      <h3 className='m-auto'>Loading</h3>
    </div>
  )
}