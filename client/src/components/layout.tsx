import React from 'react';
import { NavLink } from 'react-router-dom'

import { Container, Navbar } from 'react-bootstrap'

type Props = React.PropsWithChildren<{
  widget?: boolean
}>

const Layout: React.FC<Props> = (props: Props) => {
  const isWidget = props.widget || false

  return (
    <>
      <Navbar variant="light" className="bg-success d-flex align-items-center" id="highkick-main-nav">
        <Navbar.Brand href="" className="p-0 flex-fill">
          <img src="favicon.ico" height="32" width="32"/>
          &nbsp;
          <a href="/" className="ml-2 text-white text-monospace font-weight-bold" style={{textShadow: 'rgb(119 119 119) 1px 1px'}}>Highkick</a>
        </Navbar.Brand>

          <NavLink to="/online" className="mr-1 btn btn-link">Online</NavLink>
          <NavLink to="/index" className="mr-1 btn btn-link">Jobs</NavLink>
          <NavLink to="/jobs/stat" className="mr-1 btn btn-link">Stat</NavLink>
          {/* <NavLink to="/logs/index" className="mr-1 btn btn-link">Logs</NavLink> */}
          <NavLink to="/workers/index" className="mr-1 btn btn-link">Workers</NavLink>
          <NavLink to="/schedulers/index" className="btn btn-link">Schedulers</NavLink>
      </Navbar>
      <Container className={isWidget ? "m-0 p-0" : undefined}>
        { props.children }
      </Container>
    </>);
}

export default Layout;
