import React from 'react'
import classnames from 'classnames'

import TreeLeafInterface from '../../models/tree_leaf'
import Builder from './builder'
import Leaf from './leaf'
import BtnGroupMulti from '../../components/misc/btn_group_multiple'
import { Status, STATUSES } from '../../models/job'
import JobStatus from '../../pages/jobs/_status'

type Props<Item> = {
  items: Item[]
  builder: Builder<Item>
  isRoot?: boolean
}

export default function TreeLeaves<Item extends TreeLeafInterface>(props: Props<Item>) {
  const [filterStatuses, setFilterStatuses] = React.useState<Status[]>([])

  const finalItems = React.useMemo(() => {
    return props.items.filter(item => {
      if (filterStatuses.length > 0) {
        const matched = filterStatuses.includes(item.status)
        if (!matched) { return false }
      }
      return true
    })
  }, [props.items, filterStatuses])
  
  const activeChildStatues = React.useMemo(() => {
    return STATUSES.reduce((acc, status) => {
      const total = props.items.filter(t => t.status === status).length
      if (total > 0) {
        acc[`${status}`] = total
      }
      return acc
    }, {} as {[key: string]: number})
  }, [props.items])

  return (
    <div className='d-flex flex-column'>
      { !props.isRoot && props.items.length > 0 && (
        <div className='border rounded p-1' style={{zoom: 0.6}}>
          <BtnGroupMulti
            value={filterStatuses}
            options={Object.keys(activeChildStatues) as Status[]}
            onChange={newValue => setFilterStatuses(newValue)}
            optionRenderer={(option: Status) => {
              const total = activeChildStatues[`${option}`] || 0
              return (
                <div className='d-flex align-items-center'>
                  <JobStatus status={option}/>
                  <span className='ml-2 text-muted fw-lighter' style={{fontSize: 12}}>{option}</span>
                  <span className='ml-2 pl-2 pr-2 border rounded'>{total}</span>
                </div>)
            }}
          />
        </div>) }
      <ul className={classnames('list-group', 'list-group-flush', 'p-0')}>
        { finalItems.map(item => {
          return (
            <Leaf
              key={item.id}
              item={item}
              builder={props.builder}
            />)
        }) }
      </ul>
    </div>)
}