import React from 'react'
import _ from 'lodash'

import { Keyboard } from 'react-bootstrap-icons'

var changeThrottleTimeout: any

export enum TextInputMode {
  ChangeOnEnter,
  Throttled,
  Instant,
}

export default function TextInput(props: {
  mode: TextInputMode
  className?: string
  placeholder?: string
  defaultValue: string | undefined
  onChange: (newValue?: string) => any
}) {
  const [changed, setChanged] = React.useState(false)

  return (
    <div className={`input-group ${props.className}`}>
      <input 
        type="text" 
        className="form-control" 
        placeholder={props.placeholder}
        defaultValue={props.defaultValue} 
        onChange={e => {
          const v = _.trim(e.currentTarget.value)

          setChanged(v !== "")

          switch(props.mode) {
            case TextInputMode.Instant:
              {
                props.onChange(v)
                break;
              }
            case TextInputMode.Throttled:
              {
                clearTimeout(changeThrottleTimeout)
                changeThrottleTimeout = setTimeout(() => {
                  props.onChange(v)
                }, 2000)
                break;
              }
          }
        }}
        onKeyUp={e => {
          const v = _.trim(e.currentTarget.value)
          switch(props.mode) {
            case TextInputMode.ChangeOnEnter:
              {
                if (e.key === 'Enter' || e.keyCode === 13) {
                  props.onChange(v)
                  setChanged(false)
                }
                break;
              }
          }
        }}
        aria-describedby="button-addon1"
      />
      { (props.mode === TextInputMode.ChangeOnEnter) && (
        <button 
          className={`btn ${changed ? 'btn-primary' : 'btn-outline-secondary'}`}
          type="button" 
          id="button-addon1"
        ><Keyboard/></button>
      ) }
      
    </div>)
}