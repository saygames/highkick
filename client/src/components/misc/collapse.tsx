import React from 'react'

import { ArrowsFullscreen, FullscreenExit } from 'react-bootstrap-icons'

type Props = {
  title: React.ReactNode
  expanded?: boolean
  children: React.ReactNode
}

type State = {
  expanded: boolean
}

class Collapse extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = {
      expanded: props.expanded || false
    }
  }

  render() {
    return (
      <div className="border border-2 p-1 mb-1">
        <div
          className="zoom-on-hover"
          style={{cursor: 'pointer'}}
          onClick={() => this.setState({expanded: !this.state.expanded})}
        >
          {this.props.title}&nbsp;
          <span>{ this.state.expanded ? <FullscreenExit/> : <ArrowsFullscreen/> }</span>
        </div>
        <div className="pt-1 border-top border-2">
          {this.state.expanded && this.props.children}
        </div>
      </div>)
  }
}

export default Collapse