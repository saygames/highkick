import React from 'react'

type Props<T> = {
  options: T[]
  value: T | undefined
  onChange: (t: T | undefined) => any
  className?: string
  label?: string
}

export default function BtnGroup<T>(props: Props<T>) {
  return (
    <div className={props.className || 'd-flex'}>
      { !!props.label && (
        <button key={`label`} disabled className={`btn btn-sm me-1 btn-light border`}
        >{props.label}</button>
      ) }
      { props.options.map(option => {
        const selected = (option === props.value)
        return (
          <button
            key={`${option}`}
            className={`btn btn-sm me-1 ${selected ? 'btn-success' : 'btn-light border'}`}
            onClick={() => {
              props.onChange(option)
            }}
          >{`${option}`}</button>
        )
      }) }
    </div>
  )
}