import React from 'react'

type Props<T> = {
  title?: string
  options: T[]
  value: T[]
  onChange: (t: T[]) => any
  optionRenderer?: (t: T) => React.ReactElement
}

export default function BtnGroup<T>(props: Props<T>) {
  return (
    <div className='d-flex'>
      { !!props.title && (<button key={`title`} className={`btn btn-sm me-1 btn-secondary`} >{`${props.title}`}</button>) }
      { props.options.map(option => {
        const selected = (props.value.includes(option))
        return (
          <button
            key={`${option}`}
            className={`btn btn-sm me-1 ${selected ? 'btn-success' : 'btn-light border'}`}
            onClick={() => {
              const newValue = selected ? props.value.filter(t => t !== option) : [...props.value, option]
              props.onChange(newValue)
            }}
          >
            { !!props.optionRenderer ? props.optionRenderer(option) : `${option}` }
          </button>
        )
      }) }
    </div>
  )
}