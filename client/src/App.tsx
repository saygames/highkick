import React from 'react'
import * as ReactRedux from 'react-redux'
import ReduxState from './redux/state'

import JobMeta from './models/job_meta'
import JobMetaActions from './redux/actions/job_metas'

import { Row, Col } from 'react-bootstrap'
import Loader from './_loader'

import Layout from './components/layout'
import Router from './router'

type State = {
  loaded: boolean
}
type Props = {
  indexJobMetas?: () => any
}
class App extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      loaded: false
    };
  }

  componentDidMount() {
    (async () => {
      await this.props.indexJobMetas!()
      this.setState({loaded: true})
    })();
  }

  render() {
    return (
      <Layout>
        <Row>
          <Col md={12} className="pt-2">
            { !this.state.loaded ? <Loader/> : <Router/> }
          </Col>
        </Row>
      </Layout>);
  }
}

const mapStateToProps = (state: ReduxState, ownProps: Props) => ({})
const mapDispatchToProps = (dispatch: any, ownProps: Props) => ({
  indexJobMetas: () => dispatch(JobMetaActions.index()),
})

export default ReactRedux.connect(mapStateToProps, mapDispatchToProps)(App)
