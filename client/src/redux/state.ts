import { State as app } from './reducers/app'
import { State as jobs } from './reducers/jobs'
import { State as jobMetas } from './reducers/job_metas'

type ReduxState = {
  jobs: jobs
  jobMetas: jobMetas
  app: app
}

export default ReduxState