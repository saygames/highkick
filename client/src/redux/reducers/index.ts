import { combineReducers } from 'redux'

import app from './app'
import jobs from './jobs'
import jobMetas from './job_metas'

const rootReducer = (history: any) => 
  combineReducers({
    app,
    jobs,
    jobMetas,
  })

export default rootReducer