import ReduxState from '../state'
import Job from '../../models/job'
import Filters from '../../models/filters'

import Jobs from '../../services/jobs'

// Types

export const INDEX = 'JOBS/INDEX'
export const UPDATE = 'JOBS/UPDATE'
export const DESTROY = 'JOBS/DESTROY'

// Actions

export class Update {
  type = UPDATE
  constructor(public job: Job) { }
}

export class Index {
  type = INDEX
  constructor(public jobs: Job[]) { }
}

export class Destroy {
  type = DESTROY
  constructor(public job: Job) { }
}

export async function load(filters: Filters, params: { page: number }) {
  const jobs = await Jobs.loadRoots(filters, params)
  return jobs
}

// Action creators

export function index(filters: Filters, params: { page: number }) {
  return async (dispatch: any, getState: () => ReduxState) => {
    const jobs = await load(filters, params)
    dispatch(new Index(jobs))
  }
}

export function loadActiveRoots(filters: Filters) {
  return async (dispatch: any, getState: () => ReduxState) => {
    const roots = await Jobs.loadActiveRoots(filters)
    return roots
  }
}

export function loadSubtree(job: Job) {
  return async (dispatch: any, getState: () => ReduxState) => {
    let updatedJob = await Jobs.loadSubtree(job)
    dispatch(new Update(updatedJob)) // dispatch redux
    return updatedJob // and return value at same moment
  }
}

export function updateWithChildren(job: Job) {
  return async (dispatch: any, getState: () => ReduxState) => {
    let updatedJob = await Jobs.updateWithChildren(job)
    dispatch(new Update(updatedJob)) // dispatch redux
    return updatedJob // and return value at same moment
  }
}

export function destroy(job: Job) {
  return async (dispatch: any, getState: () => ReduxState) => {
    await Jobs.destroy(job)
    dispatch(new Destroy(job))
  }
}

export function getInput(job: Job) {
  return async (dispatch: any, getState: () => ReduxState) => {
    const input = await Jobs.getInput(job)
    return input
  }
}

export function run(props: Partial<Job>) {
  return async (dispatch: any, getState: () => ReduxState) => {
    const response = await Jobs.runJob(props)
    return response
  }
}

export function update(jobID: number, updateModel: Partial<Job>) {
	return async (dispatch: any, getState: () => ReduxState) => {
		await Jobs.update(jobID, updateModel)
	}
}

export function show(id: number) {
	return async (dispatch: any, getState: () => ReduxState) => {
		const job = await Jobs.show(id)
		return job
	}
}