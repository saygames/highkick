import React from 'react'
import Worker from '../../models/worker'

export default function Badge(props: {
  worker: Worker
}) {
  const { worker } = props
  return (
    <div className='d-flex flex-column'>
      <span>{worker.SID}</span>
      <span className='d-flex align-items-center'>
        <span>{worker.isActive() ? <>🟢</> : <>🔴</>}</span>
        <span className='flex-fill'></span>
        <span className='text-muted'>{worker.ID}</span>
      </span>
    </div>
  )
}