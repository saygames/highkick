import React from 'react'
import Moment from 'moment'
import * as ReactRedux from 'react-redux'
import { Link } from 'react-router-dom'

import ReduxState from '../../redux/state'
import WorkerActions from '../../redux/actions/workers'

import { Trash, PencilSquare, CircleFill, StopFill, PlayCircle, StopCircle, StopBtn } from 'react-bootstrap-icons'

import Worker from '../../models/worker'

type Props = {
    item: Worker
    destroy?: (item: Worker) => any // from redux
    stop: () => Promise<any> // from parent
    kill: () => Promise<any> // from parent
}

type State = {}

class WorkerComponent extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props)

        this.state = {}

        this.destroy = this.destroy.bind(this)
    }

    render() {
        const { item } = this.props
        
        return (
            <tr>
                <td>
                    {item.ID}
                </td>
                <td>
                    {item.SID}
                </td>
                <td>
                    {item.ProcessSID}
                </td>
                <td>
                    {/* {item.RunningJobsCount} */}
                    <div className="progress" style={{width: 100}}>
                        {item.RunningJobsCount > 0 ? (
                            <div className="progress-bar w-100" role="progressbar">{item.RunningJobsCount}</div>
                        ) : null}
                    </div>
                </td>
                <td>
                    {item.Stopped ? 
                        <span className="text-secondary"><StopFill/></span> :
                        <span className="text-success"><PlayCircle/></span> }
                </td>
                <td>
                    <span className={item.isActive() ? "text-success" : "text-secondary"}><CircleFill/></span>
                    {Moment(item.HealthcheckedAt).format("YYYY-MM-DD HH:mm:ss")}
                </td>
                <td>
                    {Moment(item.CreatedAt).format("MM-DD HH:mm:ss")}
                </td>
                <td>
                    {item.Stopped === true && <div className='border rounded bg-danger text-white p-1'>Will be gracefully restarted</div>}
                    {item.Stopped === false && (
                        <button 
                            className="btn btn-sm text-danger" onClick={() => this.props.stop()}
                            title='Stop & Restart'
                        ><StopCircle/></button>
                    )}
                </td>
                <td>
                    {item.Killed === true && <div className='border rounded bg-danger text-white p-1'>Will be killed</div>}
                    {item.Killed === false && (
                        <button 
                            className="btn btn-sm text-danger" onClick={() => this.props.kill()}
                            title='Force kill'
                        ><StopBtn/></button>
                    )}
                </td>
                <td>
                    <Link className="btn btn-sm" to={`/workers/edit/${item.ID}`} title="Edit"><PencilSquare/></Link>
                </td>
                <td>
                    <button className="btn btn-sm" onClick={this.destroy} title="Destroy?"
                    ><Trash/></button>
                </td>
            </tr>)
    }

    private destroy() {
        if (window.confirm('Do you wanna to destroy?') === false) {
            return
        }
        this.props.destroy!(this.props.item)
    }
}

const mapStateToProps = (state: ReduxState, ownProps: Props) => ({})
const mapDispatchToProps = (dispatch: any, ownProps: Props) => ({
    destroy: (item: Worker) => dispatch(WorkerActions.destroy(item)),
})

export default ReactRedux.connect(mapStateToProps, mapDispatchToProps)(WorkerComponent)