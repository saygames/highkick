import React from 'react'
import * as ReactRedux from 'react-redux'
import ReduxState from './../../redux/state'
import { Sun, Trash } from 'react-bootstrap-icons'

import WorkerActions from '../../redux/actions/workers'
import Worker from "../../models/worker"

import ItemComponent from './_item'
import reactKey from '../../lib/react_key'

type Props = {
    indexWorkers?: () => Promise<Worker[]>
    updateWorker?: (item: Worker) => Promise<any>
    destroyWorker?: (item: Worker) => Promise<any>
}
type State = {
    workers: Worker[]
    loading: boolean
}

class App extends React.Component<Props, State> {
    private pollInterval: null | ReturnType<typeof window.setInterval> = null
    private pulseIndicatorEl = React.createRef<HTMLDivElement>()

    constructor(props: Props) {
        super(props);
        this.state = {
            workers: [],
            loading: false,
        }
        this.loadItems = this.loadItems.bind(this)
        this.deleteDisabledWorkers = this.deleteDisabledWorkers.bind(this)
        this.touchPulseIndicator = this.touchPulseIndicator.bind(this)
        this.stopWorker = this.stopWorker.bind(this)
    }

    componentDidMount() {
        this.loadItems()
        this.pollInterval = window.setInterval(this.loadItems, 2000) as any
    }

    componentWillUnmount() {
        clearInterval(this.pollInterval as any)
    }
    
    private async loadItems() {
        this.setState({ loading: true })
        const workers = await this.props.indexWorkers!()
        this.setState({ workers, loading: false }, this.touchPulseIndicator)
    }

    private async deleteDisabledWorkers() {
        if (!window.confirm("Are you sure?")) { return }
        const disabledWorkers = this.state.workers.filter(w => !w.isActive())
        await Promise.all(disabledWorkers.map(w => this.props.destroyWorker!(w)))
        alert('Done, sir!')
    }

    private async stopWorker(worker: Worker) {
        if (!window.confirm("Gracefully Stop & Restart?")) { return }
        worker.Stopped = true;
        await this.props.updateWorker!(worker);
        alert('Done, sir!')
        await this.loadItems()
    }

    private async killWorker(worker: Worker) {
        if (!window.confirm("Force kill?")) { return }
        worker.Killed = true;
        await this.props.updateWorker!(worker);
        alert('Done, sir!')
        await this.loadItems()
    }

    render() {
        const { workers } = this.state

        return (
            <>
                <div className="mt-2 mb-2 d-flex align-items-center">
                    <h3 className="m-0 font-weight-bold mr-4 flex-fill">
                        Workers&nbsp;
                    <div 
                    className="d-inline-block pulse-indicator"
                    ref={this.pulseIndicatorEl}
                    ></div></h3>
                    <button className="btn border" onClick={this.deleteDisabledWorkers}>
                        <Trash/>&nbsp;Delete innactive
                    </button>
                </div>
                { this.state.loading && <Sun className='rotate'/> }
                { !this.state.loading && (
                    <table className="table table-sm">
                        { workers.map(worker => (
                            <ItemComponent 
                                item={worker}
                                stop={this.stopWorker.bind(this, worker)}   
                                kill={this.killWorker.bind(this, worker)}                        
                            />
                        )) }
                    </table>) }
            </>
        );
    }

    private touchPulseIndicator() {
        this.pulseIndicatorEl.current?.classList.add("active")
        setTimeout(() => {
            this.pulseIndicatorEl.current?.classList.remove("active")
        }, 500)
    }
};

const mapStateToProps = (state: ReduxState, ownProps: Props) => ({})
const mapDispatchToProps = (dispatch: any, ownProps: Props) => ({
    indexWorkers: () => dispatch(WorkerActions.index()),
    destroyWorker: (item: Worker) => dispatch(WorkerActions.destroy(item)),
    updateWorker: (item: Worker) => dispatch(WorkerActions.update(item)),
})

export default ReactRedux.connect(mapStateToProps, mapDispatchToProps)(App)
