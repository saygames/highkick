import React from 'react'
import * as ReactRedux from 'react-redux'
import { Link } from 'react-router-dom'
import Moment from 'moment'

import ReduxState from '../../redux/state'
import Actions from '../../redux/actions/schedulers'

import ReactJsonView from 'react-json-view'
import { Trash, PencilSquare } from 'react-bootstrap-icons'
import HumanDuration from '../../components/misc/human_duration'

import Scheduler, { SchedulerType } from '../../models/scheduler'

type Props = {
    item: Scheduler
    destroy?: (item: Scheduler) => any
    update?: (item: Scheduler) => any
}

type State = {
    item: Scheduler
}

class SchedulerComponent extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props)

        this.state = {
            item: props.item
        }

        this.destroy = this.destroy.bind(this)
        this.stop = this.stop.bind(this)
        this.start = this.start.bind(this)
    }

    render() {
        const { item } = this.state
        const input = JSON.parse(item.JobInput)

        return (
            <div className="card w-100">
                <div className="card-body d-flex align-items-center">
                    <span>
                        {item.JobSID}&nbsp;
                        { item.SchedulerType === SchedulerType.Timer && (
                            <>every <HumanDuration seconds={item.RunEverySeconds}/></>
                        ) }
                        { item.SchedulerType === SchedulerType.ExactTime && (
                            <>at {JSON.stringify(item.ExactTimes)}</>
                        ) }
                        { item.SchedulerType === SchedulerType.Cron && (
                            <code className='m-2'>{item.Cron}</code>
                        ) }
                    </span>
                    <div className="flex-fill">
                        <ReactJsonView src={input} collapsed={true} style={{fontSize: 10}} displayDataTypes={false}/>
                    </div>
                    { !item.Stopped && (
                        <button
                            className='btn btn-sm btn-danger m-2'
                            onClick={() => this.stop(item)}
                        >Stop</button>) }
                    { !!item.Stopped && (
                        <button
                            className='btn btn-sm btn-success m-2'
                            onClick={() => this.start(item)}
                        >Start</button>) }

                    <div className='m-2' style={{fontFamily: 'monospace'}}>
                        <small>LastRun</small><br/>
                        {Moment(item.LastRunAt).fromNow()}
                    </div>
                    { !!item.LastError && <code className="alert alert-danger">{item.LastError}</code> }

                    <Link className="btn btn-sm btn-warning" to={`/schedulers/edit/${item.ID}`}><PencilSquare/></Link>

                    <button className="btn btn-sm  btn-danger" onClick={this.destroy}
                    ><Trash/></button>
                </div>
            </div>)
    }

    private stop(scheduler: Scheduler) {
        if (window.confirm('Do you wanna to stop?') === false) {
            return
        }
        var newScheduler = new Scheduler(scheduler)
        newScheduler.Stopped = true

        this.props.update!(newScheduler)

        this.setState({
            item: newScheduler
        })
    }

    private start(scheduler: Scheduler) {
        if (window.confirm('Do you wanna to start?') === false) {
            return
        }
        var newScheduler = new Scheduler(scheduler)
        newScheduler.Stopped = false

        this.props.update!(newScheduler)

        this.setState({
            item: newScheduler
        })
    }

    private destroy() {
        if (window.confirm('Do you wanna to destroy?') === false) {
            return
        }
        this.props.destroy!(this.props.item)
    }
}

const mapStateToProps = (state: ReduxState, ownProps: Props) => ({})
const mapDispatchToProps = (dispatch: any, ownProps: Props) => ({
    destroy: (item: Scheduler) => dispatch(Actions.destroy(item)),
    update: (item: Scheduler) => dispatch(Actions.update(item)),
})

export default ReactRedux.connect(mapStateToProps, mapDispatchToProps)(SchedulerComponent)