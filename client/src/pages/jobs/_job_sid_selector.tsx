import React from 'react'
import * as ReactRedux from 'react-redux'
import ReduxState from '../../redux/state'
import JobMeta from '../../models/job_meta'
import ReactSelect from 'react-select'

type Props = {
  jobMetas?: JobMeta[]
  value: string | undefined
  onChange: (newValue: string | undefined) => any
}

class FiltersComponent extends React.Component<Props> {
  render() {
    const jobMetas = (this.props.jobMetas || []).sort((a, b) => {
      return a.SID > b.SID ? 1 : -1
    })
    const buildReactSelectOption = (v: any) => ({label: v, value: v})

    return (
      <ReactSelect
        placeholder="SID"
        isClearable
        options={(jobMetas || []).map(t => t.SID).map(buildReactSelectOption)}
        defaultValue={!!this.props.value ? buildReactSelectOption(this.props.value) : undefined}
        onChange={newValue => {
          this.props.onChange(!!newValue ? newValue.value : undefined)
        }}
      />
    )
  }
}

const mapStateToProps = (state: ReduxState, ownProps: Props) => ({
  jobMetas: state.jobMetas,
})
const mapDispatchToProps = (dispatch: any, ownProps: Props) => ({})
export default ReactRedux.connect(mapStateToProps, mapDispatchToProps)(FiltersComponent)