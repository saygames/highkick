import React, { useState } from 'react'
import Moment from 'moment'
import Datetime from "react-datetime"

import { useQueryState } from '../../lib/use_query_state'
import BtnGroupMulti from '../../components/misc/btn_group_multiple'
import BtnGroup from '../../components/misc/btn_group'

import { ChevronExpand, ChevronContract, X } from 'react-bootstrap-icons'
import Filters from '../../models/filters'
import JobSIDSelector from './_job_sid_selector'
import TextInput, { TextInputMode } from '../../components/misc/text_input'
import Worker from '../../models/worker';
import Workers from '../../services/workers'
import { STATUSES } from '../../models/job'
import WorkerBadge from '../workers/_badge'

type Props = {
  value: Filters
  onChange: (value: Filters) => any
}

export default function FiltersComponent(props: Props) {
  var { value } = props

  // const [query, setQuery] = React.useState<string | undefined>(value.Query)
  // const [sid, setSID] = React.useState<string | undefined>(value.SID)
  // const [status, setStatus] = React.useState<string | undefined>(value.Status)
  // const [since, setSince] = React.useState<string | undefined>(value.Since)
  // const [till, setTill] = React.useState<string | undefined>(value.Till)

  const [expanded, setExpanded] = useState(false)
  const [workers, setWorkers] = React.useState<Worker[]>([])

  const [query, setQuery] = useQueryState<string | undefined>({ query: "query", initialValue: value.Query })
  const [sid, setSID] = useQueryState<string | undefined>({ query: "sid", initialValue: value.SID })
  const [status, setStatus] = useQueryState<string | undefined>({ query: "status", initialValue: value.Status })
  const [since, setSince] = useQueryState<string | undefined>({ query: 'since', initialValue: value.Since })
  const [till, setTill] = useQueryState<string | undefined>({ query: 'till', initialValue: value.Till })
  const [inputContains, setInputContains] = useQueryState<string | undefined>({ query: 'input_contains', initialValue: value.InputContains })
  const [workerIDs, setWorkerIDs] = useQueryState<number[] | undefined>({ query: 'worker_ids', parser: v => {
    return !!v ? (v as any[]).map(t => parseInt(t, 10)) : undefined
  } })

  React.useEffect(() => {
    (async () => {
      let workers = await Workers.indexCached()
      workers = workers.filter(t => t.isActive())
      setWorkers(workers)
    })();
  }, []);

  React.useEffect(() => {
    const clone = Object.assign({}, value)
    clone.Query = query
    clone.SID = sid
    clone.Status = status
    clone.Since = since ? Moment(since).format("YYYY-MM-DD") + "T00:00:00Z" : undefined
    clone.Till = till ? Moment(till).format("YYYY-MM-DD") + "T00:00:00Z" : undefined
    clone.InputContains = inputContains
    clone.WorkerIDs = !!workerIDs && workerIDs.length > 0 ? workerIDs : undefined
    props.onChange(clone)
  }, [query, sid, status, since, till, inputContains, status, workerIDs])

  const buildReactSelectOption = (v: any) => ({label: v, value: v})

  return (
    <form onSubmit={e => { e.preventDefault(); return false; }}>
      <div className='d-flex flex-column'>
        <div className="d-flex align-items-center">
          <div className='mr-2' style={{maxWidth: 250}}>
            <TextInput
              placeholder='Query'
              mode={TextInputMode.Instant}
              defaultValue={query}
              onChange={newValue => {
                setQuery(!!newValue ? newValue : undefined)
              }}
            />
          </div>
          <div className='mr-2 flex-fill' style={{minWidth: 500, maxWidth: '100vw'}}>
            <JobSIDSelector
              value={sid}
              onChange={newValue => {
                setSID(newValue)
              }}
            />
          </div>
          <button className='btn btn-outline-secondary'
            onClick={() => setExpanded(!expanded)}
          >{ expanded ? <ChevronContract/> : <ChevronExpand/> }</button>
        </div>
        <div className="d-flex align-items-center">
          <BtnGroupMulti
            options={(workers || []).map(t => t.ID)}
            value={workerIDs || []}
            onChange={newValue => {
              setWorkerIDs(newValue)
            }}
            optionRenderer={(option: number) => {
              const worker = (workers || []).find(t => t.ID === option)!
              return <div style={{zoom: 0.6}}><WorkerBadge worker={worker}/></div>
            }}
          />
          <div className='flex-fill'></div>
          <BtnGroup
            options={STATUSES}
            value={value.Status}
            onChange={newValue => {
              setStatus(newValue)
            }}
          />
          <button
            key={`undefined`}
            className={`btn btn-sm me-1 btn-light border`}
            onClick={() => {
              setStatus(undefined)
            }}
          ><X/></button>
        </div>
        { expanded && (
          <div className="d-flex align-items-center">
            <div className='mr-2' style={{maxWidth: 300}}>
              <div className="d-flex align-items-center" style={{ marginLeft: 8 }}>
                <div className="fw-light" style={{ width: 125, zIndex: 1000, fontSize: 12 }}>
                  <label className='m-0 text-muted'>Since</label>
                  <Datetime key={`${since}`}
                    dateFormat="YYYY-MM-DD"
                    timeFormat={""}
                    value={since ? Moment(since) : undefined}
                    onChange={newValue => {
                      setSince(`${newValue}`)
                    }}
                  />
                </div>
                <span className="text-muted px-2">&mdash;</span>
                <div className="fw-light" style={{ width: 125, zIndex: 1000, fontSize: 12 }}>
                  <label className='m-0 text-muted'>Till</label>
                  <Datetime key={`${till}`}
                    dateFormat="YYYY-MM-DD"
                    timeFormat={""}
                    value={till ? Moment(till) : undefined}
                    onChange={newValue => {
                      setTill(`${newValue}`)
                    }}
                  />
                </div>
                <span className='btn btn-sm rounded'
                  onClick={() => {
                    setSince(undefined)
                    setTill(undefined)
                  }}
                ><X/></span>
              </div>
            </div>
            <div className='mr-2' style={{minWidth: 300, maxWidth: '50wv'}}>
              <textarea
                className='form-controll form-controll-sm'
                placeholder='Input contains'
                defaultValue={inputContains}
                onChange={e => {
                  setInputContains(e.currentTarget.value)
                }}
              />
            </div>
          </div>
        ) }
      </div>
    </form>)
}