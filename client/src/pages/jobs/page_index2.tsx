import React from 'react'
import { Link as RouterLink } from 'react-router-dom'

import { useQueryState } from '../../lib/use_query_state'
import * as Actions from '../../redux/actions/jobs'
import WorkersActions from '../../redux/actions/workers'

import { Play, Folder, FolderFill, Search, ZoomIn, Sun } from 'react-bootstrap-icons'

import Jobs from '../../services/jobs'
import Job from '../../models/job'
import JobMeta from '../../models/job_meta'
import Filters from '../../models/filters'
import Item from './_item'
import TreeLeaves from '../../components/tree/leaves'
import Paginator from '../../components/misc/paginator'
import FiltersComponent from './_filters'
import {Context} from './_context'
import * as JobGroups from './_groups'
import Worker from '../../models/worker';
import Workers from '../../services/workers'

type Props = {
  viewJSONlikeAPro?: boolean
  jobMetas?: JobMeta[]
  roots?: Job[]
  index?: (filters: Filters, params: { page: number }) => any
  changeViewJSONlikeAPro?: (newValue: boolean) => any
}

export default function RootsList(props: Props) {
  const PER_PAGE = 50
  const FOLDING_DEFAULT = false

  const [loading, setLoading] = React.useState(false)
  const [page, setPage] = React.useState(1)
  const [maxPage, setMaxPage] = React.useState(1)
  
  const [hasChanges, setHasChanges] = React.useState(false)
  const [filters, setFilters] = useQueryState<Filters>({ query: "filters", parser: v => { return v || {} } })

  const [showInput, setShowInput] = React.useState(false)
  const [folding, setFolding] = useQueryState<boolean | undefined>({ query: "folding", defaultValue: true, parser: v => !!v ? `${v}` === "true" : FOLDING_DEFAULT })

  const [groupper, setGroupper] = React.useState(new JobGroups.Groupper(filters || {}, PER_PAGE))
  const [roots, setRoots] = React.useState<Job[] | undefined>(undefined)
  const [workers, setWorkers] = React.useState<Worker[]>([])

  React.useEffect(() => {
    (async () => {
      const workers = await Workers.indexCached()
      setWorkers(workers)
    })();
  }, [])

  React.useEffect(() => {
    load(groupper.filters, page || 1)
  }, [groupper, page, folding])

  React.useEffect(() => {
    setHasChanges(true)
  }, [filters])

  const load0 = async () => {
    setHasChanges(false)
    setGroupper(new JobGroups.Groupper(filters || {}, PER_PAGE))
    setPage(1)
    await load(filters || {}, 1)
  }

  const load = async (filters: Filters, page: number) => {
    setLoading(true)

    if (folding) {
      await groupper.load(page)
      // console.log(' groupper load ', groupper)
      setGroupper(groupper) // WTF?
    } else {
      const roots = await Actions.load(filters, { page })
      setRoots(roots)
    }

    setLoading(false)
    setMaxPage(Math.max(maxPage || 1, page + 1))
  }

  const groups = groupper.getPage(page || 1)
  // console.log(groupper)

  return (
    <>
      <div className="mt-2 mb-2 d-flex flex-column ">
        <div className='d-flex align-items-center'>
          <h3 className="m-0 flex-fill font-weight-bold mr-4">Jobs</h3>
          <button className="btn btn-outline-success ml-4 font-weight-bold" title='Show input?'
            onClick={() => setShowInput(!showInput)}
          >{ showInput ? <ZoomIn/> : <Search/> }</button>
          <button className="btn btn-outline-success ml-4 font-weight-bold" title='Folding'
            onClick={() => setFolding(!folding)}
          >{ folding ? <FolderFill/> : <Folder/> }</button>
          <RouterLink to={"/new"} className="btn btn-success ml-4 font-weight-bold" title='Run new job'>
            <Play/>
          </RouterLink>
        </div>
        <div className='mt-2'>
          <FiltersComponent
            value={filters || {}}
            onChange={newValue => {
              setFilters(newValue)
            }}
          /> 
          {(hasChanges || loading) && (
            <button className='btn border rounded btn-success btn-sm mt-1 w-100' onClick={load0} disabled={loading}>
              { loading ? 'Loading ...' : 'Load'  }
            </button>
          )}
        </div>
      </div>

      { loading && (
        <div className="d-flex w-100 h-100">
          <Sun className='rotate'/>
        </div>
      ) }
      
      { !loading && (
        <Context.Provider value={{
          showInput: showInput,
          workers: workers || [],
        }}>

          { folding && (
            <div className='d-flex flex-column' key={`${page}-${groups.length}`}>
              { groups.map(group => {
                return (
                  <JobGroups.Group group={group}/>
                )
              }) }
            </div>
          ) }
          { !folding && (
            <TreeLeaves
              items={roots! || []}
              builder={Item}
              isRoot={true}
            />
          ) }
          
        </Context.Provider>)}

      <Paginator page={page || 1} maxPage={maxPage || 1} onPageChange={newValue => {
        setPage(newValue)
      }}/>
    </>)
}