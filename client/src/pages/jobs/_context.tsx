import React from 'react'
import Worker from '../../models/worker'

// DOMAIN

type Context = {
  showInput?: boolean
  workers?: Worker[]
}

// REACT

export const Context = React.createContext<Context>({})