import React from 'react'
import Moment from 'moment'
import _ from 'lodash'

import Datetime from "react-datetime"
import Chart from "react-google-charts"

import * as ReactRedux from 'react-redux'
import ReduxState from '../../redux/state'

import JobStat from '../../models/job_stat'
import JobStatService from '../../services/job_stats'
import JobSIDSelector from './_job_sid_selector'
import ErrorBoundary from '../../components/misc/error_boundary'

type Props = {}
type State = {
  loading: boolean
  items: JobStat[]
  since: Moment.Moment
  till: Moment.Moment
  filterjobSID: string | undefined
  hasChanges: boolean
}

class JobsStatPage extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)

    this.state = {
      loading: true,
      items: [],
      since: Moment().add(-3, 'days'),
      till: Moment().add(-0, 'days'),
      filterjobSID: undefined,
      hasChanges: false,
    }

    this.load = this.load.bind(this)
  }

  componentDidMount() {
    this.load().then(() => {})
  }

  render() {
    const { loading, items } = this.state

    if (loading) {
      return (
        <div className="d-flex w-100 h-100">
          <div className="m-auto">Loading</div>
        </div>
      )
    }

    const jobSIDs = _.uniq(items.map(i => i.SID)).filter(candidate => {
      if (!!this.state.filterjobSID) {
        return candidate === this.state.filterjobSID
      }
      return true
    })

    return (
      <>
        <div className="mt-2 mb-2 d-flex align-items-center">
          <h3 className="m-0 font-weight-bold mr-4">Jobs stat</h3>
          <div className="flex-fill"></div>
          <div>
            <JobSIDSelector
              value={this.state.filterjobSID}
              onChange={newValue => {
                this.setState({
                  filterjobSID: newValue,
                })
              }}
            />
          </div>
          <div className='d-flex align-items-center'>
            <Datetime
              dateFormat="YYYY-MM-DD" timeFormat={""}
              value={this.state.since}
              onChange={newValue => {
                this.setState({ since: Moment(newValue), hasChanges: true })
              }}
            />
            <span className='text-muted'>&mdash;</span>
            <Datetime
              dateFormat="YYYY-MM-DD" timeFormat={""}
              value={this.state.till}
              onChange={newValue => {
                this.setState({ till: Moment(newValue), hasChanges: true })
              }}
            />
          </div>
          { this.state.hasChanges && (
            <button 
              className='btn btn-success'
              onClick={() => this.load()}
            >Load</button>
          ) }
        </div>

        <div className='p-2 d-flex flex-column'>
          { jobSIDs.map(jobSID => {
            const data = items.filter(i => i.SID === jobSID).map(jobStat => {
              return [jobStat.Date, jobStat.Running, jobStat.Failed, jobStat.Completed]
            })
            return (
              <div className='border  m-1 p-1'>
                <ErrorBoundary>
                  <p>{jobSID}</p>
                  <Chart
                    width={'100%'}
                    height={'250px'}
                    chartType="Bar"
                    loader={<div>Loading Chart</div>}
                    data={[
                      ['Date', 'Running', 'Failed', 'Completed'],
                      ...data
                    ]}
                    options={{
                      // title: 'Population of Largest U.S. Cities',
                      // chartArea: { width: '50%' },
                      colors: ['#07A4E5', '#DA420F', '#2FAF49'],
                      isStacked: true,
                    }}
                  />
                </ErrorBoundary>
              </div>
            )
          }) }
        </div>
      </>)
  }

  private async load() {
    this.setState({ loading: true, hasChanges: false })

    const { since, till } = this.state
    const items = await JobStatService.index({
      Since: since.utc().toISOString(),
      Till: till.utc().toISOString(),
    })
    this.setState({
      loading: false,
      items,
    })
  }
}

const mapStateToProps = (state: ReduxState, ownProps: Props) => ({})
const mapDispatchToProps = (dispatch: any, ownProps: Props) => ({})

export default ReactRedux.connect(mapStateToProps, mapDispatchToProps)(JobsStatPage)