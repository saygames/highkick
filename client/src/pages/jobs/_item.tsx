import React from 'react'
import * as ReactRedux from 'react-redux'
import Moment from 'moment'
import { Link } from 'react-router-dom'

import ReduxState from '../../redux/state'
import * as Actions from '../../redux/actions/jobs'

import ReactJsonView from 'react-json-view'
import { Button } from 'react-bootstrap'
import { ArrowRight, ArrowDown, BoxArrowInRight, BoxArrowRight, Clock, } from 'react-bootstrap-icons'
import HumanDuration from '../../components/misc/human_duration'
import WorkerBadge from '../workers/_badge'

import StatusComponent from './_status'

import Job, { Status } from '../../models/job'
import JobMeta from '../../models/job_meta'
import JobLog from '../../models/job_log'
import Jobs from '../../services/jobs'
import JobLogs from '../../services/job_logs'
import {Context} from './_context'
import Graph from './_graph'

type Props = {
  item: Job
  onExpand: (expanded: boolean, updatedItem: Job | undefined) => any
  expanded: boolean

  lookupOnly?: boolean

  viewJSONlikeAPro?: boolean
  jobMetas?: JobMeta[]
  updateWithChildren?: (job: Job) => Promise<Job>
  loadSubtree?: (job: Job) => Promise<Job>
  destroy?: () => any
  getInput?: () => Promise<any>
}

type State = {
  showLogs: boolean
  jobLogs: JobLog[]
  input: any
  showInputOutput: boolean
  showGraph: boolean
  loadedSubtree: Job | undefined
}

class JobComponent extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)

    this.state = {
      showLogs: false,
      jobLogs: [],
      input: null,
      showInputOutput: false,
      showGraph: false,
      loadedSubtree: undefined,
    }

    this.loadItem = this.loadItem.bind(this)
    this.showLogs = this.showLogs.bind(this)
    this.retry = this.retry.bind(this)
    this.retryFailedLeaves = this.retryFailedLeaves.bind(this)
    this.destroy = this.destroy.bind(this)
    this.showInputOutput = this.showInputOutput.bind(this)
  }

  render() {
    const { item, expanded, jobMetas } = this.props
    const { input, jobLogs } = this.state
    const output = item.output !== "" ? JSON.parse(item.output) : {}
    const jobMeta = (jobMetas || []).find(candidate => candidate.SID === item.sid)

    const treeStatus = Jobs.treeStatus(item)
    const statusBackground = function(status: Status): string {
      return status === "failed" ? "#fcede8" : status === "processing" ? "#e8f4fc" : "#f8f9fa"
    }

    return (
      <div 
        className="p-0 m-0"
        style={{
          display: "grid",
          gridTemplateAreas: "'header actions' 'details details'",
          gridTemplateColumns: "1fr 100px",
          gridGap: "2px",
          background: statusBackground(treeStatus),
        }}
        key={JSON.stringify(jobMeta)}
      >
        <div 
          style={{
            gridArea: "header",
            cursor: 'pointer'
          }}
          className="d-flex align-items-center"
        >
          <span className="ml-1 mr-1">
            {this.renderStatus()}
          </span>
          <small className="text-muted ml-2 mr-2 fw-lighter">
            {item.id}
          </small>
          <span 
            className="ml-1 mr-1"
            onClick={this.loadItem} style={{cursor: 'pointer'}}
          >
            {jobMeta?.SID || item.sid}
            &nbsp;&nbsp;
            { !this.props.lookupOnly && (
              <>
                { expanded ? <ArrowDown/> : <ArrowRight/> }
              </>
            ) }
          </span>
          {!!item.RunWithWorker && (
            <small className="badge rounded-pill bg-light text-dark">{item.RunWithWorker}</small>
          )}          
          <div className="flex-fill">
            <Context.Consumer>{context => {
              return (
                <>
                  { context.showInput && (
                    <code style={{fontSize: 10}}>{JSON.stringify(input, null, 2)}</code>
                  ) }
                </>)
            }}</Context.Consumer>
            { (Object.keys(item.childrenStatuses || {}) as Status[]).map((key) => (
              <>
                { (item.childrenStatuses[key] > 0) && (
                  <span className="badge" style={{background: statusBackground(key)}}>
                    { key } <span className="badge badge-light">{item.childrenStatuses[key]}</span>
                  </span>
                ) }
              </>
            )) }
          </div>
          <div className="d-flex align-items-center text-muted text-nowrap">
            {!this.props.lookupOnly && (
              <Button variant="outline-light" size="sm"
                style={{fontSize: 12}}
                className={`fw-lighter ${this.state.showInputOutput ? "text-dark" : "text-muted"}`}
                onClick={() => this.showInputOutput(!this.state.showInputOutput)}
              >Input / output</Button>) }
            {!this.props.lookupOnly && (item.logsCount > 0) && (
              <Button variant="outline-light" size="sm"
                style={{fontSize: 12}}
                className={`fw-lighter ${this.state.showLogs ? "text-dark" : "text-muted"}`}
                onClick={() => this.showLogs(!this.state.showLogs)}
              >Logs</Button>
            ) }
            {!this.props.lookupOnly && (
              <Button variant="outline-light" size="sm"
                style={{fontSize: 12}}
                className={`fw-lighter ${this.state.showGraph ? "text-dark" : "text-muted"}`}
                onClick={() => this.showGraph(!this.state.showGraph)}
              >✡</Button>
            ) }
            <div className='d-flex align-items-center text-nowrap ms-1 me-1 border rounded p-1' style={{fontSize: 12}}>
              {Moment(item.createdAt).format("MM-DD HH:mm")}
              { item.durationSeconds() > 0 && (
                <div className='d-flex align-items-center ms-1'>
                  <Clock/>
                  <HumanDuration seconds={item.durationSeconds()}/>
                </div>) }
            </div>
          </div>
          <Context.Consumer>{context => {
            const worker = (context.workers || []).find(t => t.ID === item.WorkerID)
            if (!!worker) return <div className='border rounded m-1 p-1' style={{zoom: 0.5}}><WorkerBadge worker={worker}/></div>
            return (
              <small className="text-muted">
                <span className="badge bg-secondary text-white ml-1 fw-light">
                  <span>{item.WorkerID}</span>
                </span>
              </small>)
          }}</Context.Consumer>
          
        </div>

        { !this.props.lookupOnly && (
          <div style={{ gridArea: "actions" }}
          >
            <Button variant="ouline-light" size='sm'
              onClick={this.retry}
            >🔁</Button>
            <Link to={`/jobs/edit/${item.id}`} className="btn btn-ouline-light btn-sm"
            >📝</Link>
            <Button variant="ouline-light" onClick={this.destroy} size='sm'
            >🗑️</Button>
          </div>) }

        <div
          style={{ 
            gridArea: "details",
          }}
          className="d-flex flex-column"
        >
          <div
            style={{
              display: this.state.showInputOutput ? 'flex' : 'none'
            }}
          >
            <div className="d-flex align-items-center">
              <BoxArrowInRight className="m-2" style={{zoom: 1.5}}/>
              
              { !!input && <ReactJsonView src={input} collapsed={true} style={{fontSize: 10}} displayDataTypes={false}/> }
              { !input && <>&mdash;</> }

              {/* { !this.props.viewJSONlikeAPro && (
                <code style={{fontSize: 10}}>{JSON.stringify(input, null, 2)}</code>
              ) } */}
            </div>
            <div className="d-flex align-items-center">
              <BoxArrowRight className="m-2" style={{zoom: 1.5}}/>

              { !!output && <ReactJsonView src={output} collapsed={true} style={{fontSize: 10}} displayDataTypes={false}/> }
              { !output && <>&mdash;</> }

              {/* { !this.props.viewJSONlikeAPro && (
                <code style={{fontSize: 10}}>{JSON.stringify(output, null, 2)}</code>
              ) } */}
            </div>
          </div>
          
          { this.state.showGraph && !!this.state.loadedSubtree && (
            <div className='p-2 border m-2'>
              <Graph job={this.state.loadedSubtree}/>
            </div>) }

          { this.state.showLogs && (
            <div className='p-2 border m-2'>
              { jobLogs.map(jobLog => {
                return (
                  <div className="alert alert-primary p-0 d-flex" key={jobLog.id}>
                    <small className="text-muted mr-2">{Moment(jobLog.createdAt).format("YYYY-MM-DD HH:mm:ss")}</small>
                    <code className="flex-fill">{jobLog.content}</code>
                  </div>)
              }) }
            </div>
          ) }
          
        </div>
      </div>)
  }

  private renderStatus() {
    const { item } = this.props
    const treeStatus = Jobs.treeStatus(item)

    if (item.status === treeStatus) {
      return <StatusComponent status={item.status}/>
    }

    return [
      <StatusComponent status={treeStatus}/>,
      <StatusComponent status={item.status}/>
    ]
  }

  private async loadItem() {
    const { item, expanded } = this.props
    if (!expanded) {
      const updatedJob = await this.props.updateWithChildren!(item)
      this.props.onExpand(true, updatedJob)
    } else {
      this.props.onExpand(false, undefined)
    }
  }

  private async showLogs(showLogs: boolean) {
    const { item } = this.props
    if (showLogs) {
      let jobLogs = await JobLogs.loadLogsByJobId(item.id)
      this.setState({ showLogs, jobLogs })
    } else {
      this.setState({ showLogs })
    }
  }

  private async showGraph(newValue: boolean) {
    const { item } = this.props
    switch(newValue) {
      case true:
        {
          const loadedSubtree = await this.props.loadSubtree!(item)
          this.setState({ showGraph: true, loadedSubtree })
          break;
        }
      case false:
        {
          this.setState({ showGraph: false })
          break;
        }
    }
  }

  private retry() {
    const { item } = this.props;
    if (window.confirm('Do you wanna to retry this job?') === false) {
      return
    }

    (async () => {
      await Jobs.retry(item)
    })()
  }

  private retryFailedLeaves() {
    const { item } = this.props;
    if (window.confirm('Do you wanna to retry failed children of this job?') === false) {
      return
    }

    (async () => {
      await Jobs.retryFailedLeaves(item)
    })()
  }

  private destroy() {
    if (window.confirm('Do you wanna to destroy this job?') === false) {
      return
    }
    this.props.destroy!()
  }

  private async showInputOutput(showInputOutput: boolean) {
    if (showInputOutput === true) {
      const input = await this.props.getInput!()
      this.setState({ input })
    }
    this.setState({ showInputOutput })
  }
}

const mapStateToProps = (state: ReduxState, ownProps: Props) => ({
  jobMetas: state.jobMetas,
  viewJSONlikeAPro: state.app.viewJSONlikeAPro,
})
const mapDispatchToProps = (dispatch: any, ownProps: Props) => {
  const { item } = ownProps
  return {
    updateWithChildren: (job: Job) => dispatch(Actions.updateWithChildren(job)),
    loadSubtree: (job: Job) => dispatch(Actions.loadSubtree(job)),
    destroy: () => dispatch(Actions.destroy(item)),
    getInput: () => dispatch(Actions.getInput(item)),
  }
}

export default ReactRedux.connect(mapStateToProps, mapDispatchToProps)(JobComponent)