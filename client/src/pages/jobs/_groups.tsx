import React from 'react'

import Job from '../../models/job'
import Filters from '../../models/filters'
import Jobs from '../../services/jobs'

import TreeLeaves from '../../components/tree/leaves'
import Item from './_item'

export class Groupper {
  perPage: number
  filters: Filters

  groups: Job[][]
  innerPage: number = 0
  
  constructor(filters: Filters, perPage: number) {
    this.filters = Object.assign({}, filters)
    this.perPage = perPage
    this.groups = []
  }

  getPage(page: number) {
    return this.groups.slice((page - 1)*this.perPage, page*this.perPage)
  }

  async load(page: number) {
    while(true) {
      // break condition
      if (this.getPage(page).length === this.perPage) { break }

      // load more
      this.innerPage += 1
      const moreJobs = await Jobs.loadRoots(this.filters, { page: this.innerPage })
      if (moreJobs.length === 0) { break }

      this.append(moreJobs)
    }
  }

  append(jobs: Job[]) {
    for(var job of jobs) {
      if (this.groups.length === 0) {
        this.groups.push([job])
        continue
      }

      var lastGroupSID = this.groups[this.groups.length - 1][0].sid
      if (lastGroupSID === job.sid && !this.shouldNotGroup()) {
        this.groups[this.groups.length - 1].push(job)
      } else {
        this.groups.push([job])
      }
    }

    // console.log('appended ', jobs.length, this.groups)
  }

  shouldNotGroup() {
    return !!this.filters.SID
  }
}

export function Group(props: {
  group: Job[]
}) {
  const [expanded, setExpanded] = React.useState(false)

  const finalExpanded = props.group.length === 1 ? true : expanded

  return (
    <div className=''>
      <div className='d-flex w-100'>
        { props.group.length > 1 && (
          <button
            className='btn btn-light me-2'
            onClick={() => setExpanded(!expanded)}
          >{props.group.length}&nbsp;{expanded ? <>⬆️</> : <>↕️</>}</button>) }
        <div className='flex-fill'>
          { !finalExpanded && (
            <Item item={props.group[0]} onExpand={() => {}} expanded={false} lookupOnly/>
          ) }
          { finalExpanded && (
            <TreeLeaves
              items={props.group}
              builder={Item}
            />
          ) }
        </div>
      </div>
    </div>)
}
