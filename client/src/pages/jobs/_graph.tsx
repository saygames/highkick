import React from 'react'

import Graphology from "graphology";
import GraphologyLayoutRandom from 'graphology-layout/random';
import GraphologyLayoutNoverlap from 'graphology-layout-noverlap'

import { SigmaContainer } from "@react-sigma/core";

import "@react-sigma/core/lib/react-sigma.min.css"

import Job from '../../models/job'

enum Mode {
  Full = 'Full',
  Compacted = 'Compacted',
}

export default function JobGraph(props: {
  job: Job
}) {
  const [mode, setMode] = React.useState(Mode.Full)
  const [graph, setGraph] = React.useState<Graphology>()

  const getNode = (job: Job) => {
    switch(mode) {
      case Mode.Full:
        return `${job.id} - ${job.sid}`
      case Mode.Compacted:
        return `${job.sid}`
    }
  }

  React.useEffect(() => {
    const graph = new Graphology()

    const dfs = (job: Job) => {
      var parentNode = getNode(job)
      graph.mergeNode(parentNode, { label: parentNode }) // mergeNode - Adds a node only if the node does not exist in the graph yet
      job.childs.map(child => {
        const childNode = getNode(child)
  
        graph.mergeNode(childNode, { label: childNode })  // mergeNode - Adds a node only if the node does not exist in the graph yet
        graph.mergeEdge(parentNode, childNode) // mergeEdge - - Adds only if does not exist
  
        dfs(child)
      })
    }
    
    dfs(props.job)

    GraphologyLayoutRandom.assign(graph)
    GraphologyLayoutNoverlap.assign(graph)

    setGraph(graph)
  }, [mode])

  return (
    <div className='d-flex flex-column'>
      <div className='d-flex'>
        <span>Graph</span>
        <div className='d-flex'>
          { [Mode.Full, Mode.Compacted].map(_mode => {
            return (
              <button 
                className={`btn btn-sm ${mode === _mode ? 'btn-success' : 'btn-outline-success'}`}
                onClick={() => setMode(_mode)}
              >{_mode}</button>
            )
          }) }
        </div>
      </div>
      <SigmaContainer 
        key={`${mode}`}
        style={{ height: "500px", width: "100%" }} 
        graph={graph}
        initialSettings={{
          allowInvalidContainer: true,
          labelRenderedSizeThreshold: 0,
        }}
      />
    </div>
  )
}