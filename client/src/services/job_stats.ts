import JobStat from '../models/job_stat'

import API from './api'
import HTTP from '../lib/http'

export type Filters = Partial<{
  Since: string
  Till: string
}>

async function index(filters: Filters) {
  const url = API.URLS.jobStats.index
  const response = await HTTP.get(url, { filters })
  const items = (response.items as any[]).map(JobStat.deserialize)
  return items
}

export default { index }
