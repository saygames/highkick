import Job, { Status } from '../models/job'
import Filters from '../models/filters'

import API from './api'
import HTTP from '../lib/http'

import Tree from './tree'

async function loadRoots(filters: Filters, params: { page: number }) {
  const data = Object.assign({}, params, { filters })
  if (!!data.filters && !!data.filters.WorkerIDs) {
    data.filters.WorkerIDs = (data.filters.WorkerIDs || []).map(t => parseInt(`${t}`, 10))
  }
  const url = API.URLS.jobRoots.index
  const rootJsons = await HTTP.get(url, data)
  const roots = rootJsons.map(Job.deserialize)
  return roots
}

async function loadActiveRoots(filters: Filters) {
  const data = Object.assign({}, { filters })
  const url = API.URLS.jobRoots.active
  const responseJson = await HTTP.get(url, data)
  const childrenStats= responseJson.ChildrenStats as {RootID: number, ChildrenStatuses: {[status: string]: number}}[]
  
  let roots = (responseJson.Items as any[]).map(Job.deserialize)
  roots = roots.map(root => {
    root.childrenStatuses = childrenStats.find(cs => cs.RootID === root.id)!.ChildrenStatuses
    return root
  })

  return roots
}

async function loadSubtree(job: Job) {
  const jsons = await HTTP.get(API.URLS.jobs.subtree(job.id))
  const jobs = jsons.map(Job.deserialize)
  const updatedJob = Tree.compose<Job>({
    items: jobs,
    rootId: job.id
  })
  return updatedJob
}

async function updateWithChildren(job: Job) {
  const jsons = await HTTP.get(API.URLS.jobs.children(job.id))
  const jobs = jsons.map(Job.deserialize)
  
  const newJob = new Job(job)
  newJob.childs = jobs
  return newJob
}

async function retry(job: Job) {
  await HTTP.post(API.URLS.jobs.retry(job.id))
}

async function retryFailedLeaves(job: Job) {
  await HTTP.post(API.URLS.jobs.retryFailedLeaves(job.id))
}

async function destroy(job: Job) {
  const url = API.URLS.jobs.destroy(job.id)
  await HTTP.del(url)
}

function treeStatus(job: Job): Status {
  const statuses = job.childs.map(treeStatus)
  statuses.push(job.status)
  if (!!job.treeStatus) { statuses.push(job.treeStatus) }

  if (statuses.some(s => s === Status.processing)) {
    return Status.processing
  }

  if (statuses.some(s => s === Status.failed)) {
    return Status.failed
  }

  if (statuses.every(s => s === Status.completed)) {
    return Status.completed
  }

  if (statuses.every(s => s === Status.initial)) {
    return Status.initial
  }

  return Status.processing
}

async function getInput(job: Job) {
  const url = API.URLS.jobs.input(job.id)
  const data = await HTTP.get(url)
  return data
}

async function runJob(props: Partial<Job>) {
  const url = API.URLS.jobs.run
  const response = await HTTP.post(url, {
    SID: props.sid,
    Input: props.input,
    RunWithWorker: props.RunWithWorker,
  })
  return response
}

async function update(jobID: number, job: Partial<Job>) {
  const data = Object.assign({}, job)
  const url = API.URLS.jobs.update(jobID)
  await HTTP.post(url, data)
}

async function show(id: number) {
  const data = Object.assign({}, {})
  const url = API.URLS.jobs.show(id)
  const response = await HTTP.get(url, data)
  return new Job(response.job)
}

export default { 
  loadRoots, loadSubtree, retry, retryFailedLeaves,
  destroy, treeStatus, getInput,
  runJob,
  loadActiveRoots,
  update, show,
  updateWithChildren,
}
