import React from 'react'
import { Routes, Route, Navigate } from 'react-router'

import Online from './pages/jobs/page_online'

import JobsIndex from './pages/jobs/page_index'
import JobsIndex2 from './pages/jobs/page_index2'

import JobsNew from './pages/jobs/page_new'
import JobsEdit from './pages/jobs/page_edit'
import JobsStat from './pages/jobs/page_stat'

import LogsIndex from './pages/logs/index'

import SchedulersIndex from './pages/schedulers/schedulers_index'
import SchedulersNew from './pages/schedulers/schedulers_new'
import SchedulersEdit from './pages/schedulers/schedulers_edit'

import WorkersIndex from './pages/workers/index'
import WorkersEdit from './pages/workers/edit'

function Router() {
  return (
    <Routes>
      <Route path="/index" element={<JobsIndex2 />}/>
      <Route path="/online" element={<Online />}/>
      <Route path="/new" element={<JobsNew />}/>
      <Route path="/jobs/edit/:id" element={<JobsEdit />}/>
      <Route path="/jobs/stat" element={<JobsStat />}/>
      <Route path="/schedulers/index" element={<SchedulersIndex />}/>
      <Route path="/schedulers/new" element={<SchedulersNew />}/>
      <Route path="/schedulers/edit/:id" element={<SchedulersEdit />}/>
      <Route path="/workers/index" element={<WorkersIndex />}/>
      <Route path="/workers/edit/:id" element={<WorkersEdit />}/>
      <Route path="/logs/index" element={<LogsIndex />}/>
      <Route path="/" element={<Navigate replace to="/index" />} />
    </Routes>
  )
}

export default Router