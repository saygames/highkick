import React from 'react';
import { createRoot } from 'react-dom/client'

import * as serviceWorker from './serviceWorker';

import "@fontsource/montserrat"
import "./styles.css"
import "react-datetime/css/react-datetime.css";

import { Provider } from 'react-redux'
import { HashRouter } from "react-router-dom"
import { store } from './redux/store'

import App from './app';

const container = document.getElementById('root')!;
const root = createRoot(container); // createRoot(container!) if you use TypeScript
root.render(
  <HashRouter>
    <Provider store={store}>
      <App />
    </Provider>
  </HashRouter>
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
