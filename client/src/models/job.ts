import TreeLeaf from './tree_leaf'
import Moment from 'moment'

export enum Status {
  scheduled = 'scheduled',
  initial = 'initial',
  processing = 'processing',
  failed = 'failed',
  completed = 'completed'
}

export const STATUSES = [Status.initial, Status.scheduled, Status.processing, Status.completed, Status.failed]

type Props = {
  id: number
  sid: string
  path: string
  input: string
  output: string
  status: Status
  treeStatus?: Status
  createdAt: string
  cron?: string
  logsCount: number
  RunWithWorker: string
  WorkerID: number
  StartedAt: string
  FinishedAt: string

  childs: Job[]
  childrenStatuses: {[status: string]: number}
}

class Job implements Props, TreeLeaf {
  id: number = 0
  sid: string = ''
  path: string = ''
  input: string = ''
  output: string = ''
  status: Status = Status.initial
  treeStatus?: Status = undefined
  createdAt: string = ''
  cron?: string = undefined
  logsCount: number = 0
  RunWithWorker: string = ''
  WorkerID: number = 0
  StartedAt: string = ''
  FinishedAt: string = ''

  childs: Job[] = []
  childrenStatuses: {[status: string]: number} = {}

  constructor(props: Partial<Props>) {
    // super()
    for(const prop in props) {
      (this as any)[prop] = (props as any)[prop]
    }
  }

  isRoot() {
    return this.path === ''
  }

  isPeriodical() {
    return this.cron !== undefined
  }

  parentID() {
    if (this.isRoot()) {
      return null
    }
    const ids = this.path.split('/').map(i => parseInt(i))
    return ids[ids.length - 1]
  }

  // TODO: use hash function
  digest(): string {
    const childsDigest = this.childs.map(c => c.digest()).join()
    return `${this.id}${this.status}${this.treeStatus}${childsDigest}`
  }

  static deserialize(json: any): Job {
    const job = new Job(json as Partial<Props>)
    return job
  }

  durationSeconds() {
    if (!this.StartedAt || !this.FinishedAt) { return 0 }
    var durMilisecs = Moment(this.FinishedAt).diff(Moment(this.StartedAt))
    return durMilisecs / 1000
  }
}

export default Job