type Filters = Partial<{
  Query: string
  SID: string
  Status: string
  Since: string
  Till: string
  InputContains: string
  WorkerIDs: number[]
}>

export default Filters