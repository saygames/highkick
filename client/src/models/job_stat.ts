class JobStat {
  SID!: string
  Date!: string
  Running!: number
  Failed!: number
  Completed!: number

  constructor(props: Partial<JobStat>) {
    // super()
    for(const prop in props) {
      (this as any)[prop] = (props as any)[prop]
    }
  }

  static deserialize(json: any): JobStat {
    const result = new JobStat(json as Partial<JobStat>)
    return result
  }
}

export default JobStat