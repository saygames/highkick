import { Status } from "./job"

interface TreeLeaf {
  id: number
  childs: TreeLeaf[]

  isRoot: () => boolean
  parentID: () => number | null
  digest: () => string
  status: Status
}

export default TreeLeaf