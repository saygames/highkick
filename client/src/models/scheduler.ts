export enum SchedulerType {
    Timer = "timer",
    ExactTime = "exact_time",
    Cron = "cron"
}

class Scheduler {
    ID!: number
    JobSID!: string
    JobInput!: string

    SchedulerType!: SchedulerType
    // SchedulerType=Timer
    RunEverySeconds!: number
    // SchedulerType=ExactTime
    ExactTimes!: string[]
    // SchedulerType=Cron
    Cron!: string

    Stopped!: boolean
    UpdatedAt!: string
    
    LastRunAt!: string
    LastError!: string

    constructor(props: Partial<Scheduler>) {
        for(const prop in props) {
            (this as any)[prop] = (props as any)[prop]
        }
    }
}

export default Scheduler