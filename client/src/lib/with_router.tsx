import React from 'react'
import { useLocation, useParams, useNavigate, Location, Params, NavigateFunction } from "react-router-dom"

export const withRouter = (Component: React.ComponentClass) => (props: any) => {
  const location = useLocation();
  const params = useParams();
  const navigate = useNavigate();

  return <Component {...props} {...{ location, params, navigate }} />;
};

export type Props = Partial<{
  location: Location,
  params: Params,
  navigate: NavigateFunction,
}>