import React, { useState } from 'react';

// Code written by OpenGPT

interface CronScheduleEditorProps {
  value: string;
  onChange: (value: string) => void;
}

const validateSchedule = (schedule: string) => {
  const regex = /^(\*|(?:[0-9]|[1-5][0-9]))(\-(\*|(?:[0-9]|[1-5][0-9])))?((\/|\-|\,)(\*|(?:[0-9]|[1-5][0-9])))?((\/|\-|\,)(\*|(?:[0-9]|[1-5][0-9])))?((\/|\-|\,)(\*|(?:[0-9]|[1-5][0-9])))?((\/|\-|\,)(\*|(?:[0-9]|[1-5][0-9])))?$/;
  const regex2 = /^(\*|([0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9])|\*\/([0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9])) (\*|([0-9]|1[0-9]|2[0-3])|\*\/([0-9]|1[0-9]|2[0-3])) (\*|([1-9]|1[0-9]|2[0-9]|3[0-1])|\*\/([1-9]|1[0-9]|2[0-9]|3[0-1])) (\*|([1-9]|1[0-2])|\*\/([1-9]|1[0-2])) (\*|([0-6])|\*\/([0-6]))$/
  return regex2.test(schedule);
};

const CronScheduleEditor: React.FC<CronScheduleEditorProps> = ({ value, onChange }) => {
  const [schedule, setSchedule] = useState(value);
  const [isValid, setIsValid] = useState(validateSchedule(value));

  const handleScheduleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const newSchedule = event.target.value;
    const newIsValid = validateSchedule(newSchedule);
    setSchedule(newSchedule);
    setIsValid(newIsValid);
    if (newIsValid) {
      onChange(newSchedule);
    }
  };

  return (
    <div className='form-group'>
      <label>Cron Schedule</label>
      <input type="text" value={schedule} onChange={handleScheduleChange} className="form-control"/>
      {!isValid && <div className='p-1 bg-danger text-white rounded fw-lighter'>Please enter a valid cron schedule</div>}
    </div>
  );
};

export default CronScheduleEditor;