import React from "react"
import { useNavigate, useLocation } from "react-router-dom"

import qs from "qs"

type Params<V> = {
  query: string
  initialValue?: V
  defaultValue?: V
  parser?: (t: any) => V | undefined
}

export const useQueryState = function<V>(params: Params<V>): [V | undefined, (v: V) => any] {
  const location = useLocation()
  const navigate = useNavigate()

  const [inited, setInited] = React.useState(false)

  let currentValue = React.useMemo(() => {
    const query = qs.parse(location.search, { ignoreQueryPrefix: true })[params.query]
    let value = !!params.parser ? params.parser!(query) : (query as unknown as V)
    if (!inited && !!params.initialValue && value === undefined) { value = params.initialValue; setInited(true) }
    if (value === undefined) { value = params.defaultValue }
    return value
  }, [location.search, location.pathname])

  const setter = React.useCallback((newValue: V) => {
    const existingQueries = qs.parse(location.search, {
      ignoreQueryPrefix: true,
    })

    const queryString = qs.stringify(
      { ...existingQueries, [params.query]: newValue },
      { skipNulls: true }
    )

    if (currentValue !== newValue) {
      navigate(`${location.pathname}?${queryString}`, {
        replace: false
      })
    }
    
  }, [navigate, location, params])

  return [
    currentValue,
    setter,
  ]
}