export function hashCode(str: string) {
  return Array.from(str)
    .reduce((s, c) => Math.imul(31, s) + c.charCodeAt(0) | 0, 0)
}

export default function reactKey(...args: any[]) {
  return hashCode(`${args.map(arg => JSON.stringify(arg))}`)
}