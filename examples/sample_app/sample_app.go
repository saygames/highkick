package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"sync"
	"time"

	"bitbucket.org/saygames/highkick"
	"bitbucket.org/saygames/highkick/pkg/wrap_func"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"

	_ "github.com/go-sql-driver/mysql"
)

const HELLO_WORLD = "HELLO_WORLD"

type HelloWorldWorkerInput struct {
	N     int
	Depth int
}
type HelloWorldWorkerOutput struct {
	Result int
}

func HelloWorldWorker(job *highkick.Job) error {
	input := HelloWorldWorkerInput{}
	if err := json.Unmarshal([]byte(*job.Input), &input); err != nil {
		return err
	}

	output := HelloWorldWorkerOutput{}
	defer func() {
		highkick.SetOutputRaw(job, output)
	}()

	highkick.Log(job, fmt.Sprintf("Log"))

	output.Result = input.N * input.N

	if input.Depth > 0 {
		job2, err := highkick.RunSync(highkick.NewJob2(HELLO_WORLD, HelloWorldWorkerInput{
			N:     input.N * 2,
			Depth: input.Depth - 1,
		}, job))
		if err != nil {
			return err
		}
		highkick.Log(job, fmt.Sprintf("Running child job %v", job2.ID))
	}

	return nil
}

func init() {
	inputJSONSchema, err := highkick.GetJSONSchemaFor(&HelloWorldWorkerInput{})
	if err != nil {
		panic(err)
	}
	highkick.Register(highkick.JobMeta{
		SID:             HELLO_WORLD,
		Title:           HELLO_WORLD,
		Perform:         HelloWorldWorker,
		InputJSONSchema: inputJSONSchema,
	})
}

func main() {
	fmt.Println("Starting")

	const DSN = "root:12345@tcp(127.0.0.1:3306)/highkick_dev?clientFoundRows=true&charset=utf8mb4&parseTime=true&multiStatements=true"
	db, err := sql.Open("mysql", DSN)
	// db, err := sql.Open("sqlite3", "./highkick.db?parseTime=true")
	if err != nil {
		panic(err)
	}

	highkick.SetupDatabase(highkick.DatabaseOptions{
		DB:            db,
		Engine:        highkick.DatabaseEngines.MySQL,
		Database:      "highkick_dev",
		RunMigrations: true,
		EnableLogging: false,
	})

	host, err := os.Hostname()
	if err != nil {
		panic(err)
	}

	highkickDebugLog := func(msg string) {
		fmt.Printf("[Highkick]: %v\n", msg)
	}

	worker, err := highkick.RegisterAndRunWorker(highkick.Worker{
		SID:        "MAIN_WORKER",
		ProcessSID: fmt.Sprintf("%v-%v", host, os.Getpid()),
	}, highkick.WorkerParams{
		JobsToHandle: highkick.JobsToHandle{},
		DebugLog:     highkickDebugLog,
	})
	if err != nil {
		panic(err)
	}
	highkick.RunSchedulers(*worker, highkick.RunSchedulersParams{
		JobsToHandle:        highkick.JobsToHandle{},
		DebugLog:            highkickDebugLog,
		PollIntervalSeconds: 5,
	})
	highkick.RunWorkerMonitor(worker.ID, func() {
		os.Exit(0)
	}, highkickDebugLog)

	// In-process pubsub
	highkick.JobsPubSub.Subscribe(func(iMessage interface{}) {
		message := iMessage.(highkick.PubSubMessage)
		fmt.Printf("Job %v (%+v) completed with %v error\n", message.Job.Sid, message.Job.GetInput(), message.Error)
	})

	gin.SetMode(gin.ReleaseMode)
	engine := gin.Default()
	engine.Use(cors.Default())

	engine.Static("/app", ".")

	// highkickAuth := gin.BasicAuth(gin.Accounts{"foo": "bar"})

	{
		highkick.RunServer(engine, highkick.ServerParams{
			// AuthMiddleware: &highkickAuth,
			ClientURL: "/highkick_app",
		})
		go func() {
			if err := engine.Run("0.0.0.0:8000"); err != nil {
				log.Fatalln(err)
			}
		}()

		fmt.Println("Server running on http://localhost:8000/app")
	}

	// USAGE

	// // Case 0
	// {
	// 	j1 := domain.Job{ID: 9}
	// 	j2 := domain.NewJob2("A", nil, &j1)
	// 	fmt.Println("GetParentID", j2.GetParentID())
	// 	fmt.Println("FullPath", j2.FullPath)
	// 	p := j2.FullPathWithoutZeros()
	// 	fmt.Println("FullPathWithoutZeros", p)
	// 	fmt.Println("len(p)", len(p))
	// 	fmt.Println("len(p)", p[len(p)-1])
	// 	os.Exit(9)
	// }

	// // Case 1. Run in async way. Will be runned by worker launcher
	// {
	// 	highkick.RunAsync(highkick.NewJob(HELLO_WORLD, highkick.Input{
	// 		"Depth": 1,
	// 	}, nil))

	// 	output, err := intercom.RunAndPoll[HelloWorldWorkerOutput](HELLO_WORLD, HelloWorldWorkerInput{
	// 		N: 3,
	// 	}, intercom.RunAndPollParameters{})
	// 	if err != nil {
	// 		panic(err)
	// 	}
	// 	fmt.Printf("RunAndPoll: %v\n", *output)
	// }

	// Case 2. Function wrapping
	{
		type Input struct {
			A int
			B int
		}
		type Output struct {
			C int
		}

		f := func(input Input, meta wrap_func.ExecutionMeta) (*Output, error) {
			meta.Log("Yo")
			time.Sleep(1 * time.Second)
			meta.Log("Yo2")

			if input.A == 2 {
				return nil, fmt.Errorf("A === 2")
			}

			return &Output{
				C: input.A + input.B,
			}, nil
		}

		fmt.Printf("Case2. Function call A+B=C. A = 2 B = 2 \n")

		output, err := wrap_func.Run(f, wrap_func.RunParams[Input]{
			Input: Input{
				A: 2,
				B: 2,
			},
			SaveOutput: true,
		})

		fmt.Printf("Error = %v\n", err)
		fmt.Printf("Result = %v\n", output)
		// fmt.Printf("Result = %v\n", *output)
	}

	wg := sync.WaitGroup{}
	wg.Add(1)
	wg.Wait()
}
