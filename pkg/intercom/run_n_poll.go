package intercom

import (
	"fmt"
	"net/http"
	"time"

	"bitbucket.org/saygames/highkick/internal/domain"
	"bitbucket.org/saygames/highkick/pkg/http_intercom"
)

type HttpConnectionParams struct {
	Host     string
	Timeout  time.Duration
	AuthFunc func(req *http.Request)
}

type RunAndPollParameters struct {
	Http            http_intercom.HttpConnectionParams
	IntervalSeconds float64
	MaxTries        int
	DebugLog        *func(string)
}

func RunAndPoll[iJobOutput interface{}](sid string, input interface{}, params RunAndPollParameters) (*iJobOutput, error) {
	httpClient := http_intercom.NewHttpClient(params.Http)

	// Params sanitizing
	debugLog := func(msg string) {
		fmt.Println(msg)
	}
	if params.DebugLog != nil {
		debugLog = *params.DebugLog
	}

	intervalSeconds := 10.0
	if params.IntervalSeconds > 0 {
		intervalSeconds = params.IntervalSeconds
	}

	maxTries := 100
	if params.MaxTries > 0 {
		maxTries = params.MaxTries
	}

	// Run
	v, err := domain.StructToJSONDictionary(input)
	if err != nil {
		return nil, err
	}
	newJobID, err := httpClient.RunJob(sid, *v)
	if err != nil {
		return nil, err
	}

	// Wait a little
	time.Sleep(1 * time.Second)

	// Poll
	newJob, err := httpClient.PollJobStatus(newJobID, http_intercom.PollJobStatusParams{
		Limit:           maxTries,
		IntervalSeconds: intervalSeconds,
		DebugLog:        debugLog,
	})
	if err != nil {
		return nil, err
	}
	var output iJobOutput
	if err := newJob.CastOutput(&output); err != nil {
		return nil, err
	}
	return &output, nil
}
