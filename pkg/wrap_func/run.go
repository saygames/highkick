package wrap_func

import (
	"fmt"
	"reflect"
	"runtime"

	"bitbucket.org/saygames/highkick/internal/domain"
	"bitbucket.org/saygames/highkick/internal/usecases"
)

type ExecutionMeta struct {
	Log func(string)
}

type Function[Input interface{}, Output interface{}] func(Input, ExecutionMeta) (*Output, error)

type RunParams[Input interface{}] struct {
	Input      Input
	SaveOutput bool
}

func Run[Input interface{}, Output interface{}](f Function[Input, Output], params RunParams[Input]) (*Output, error) {

	functionName := runtime.FuncForPC(reflect.ValueOf(f).Pointer()).Name()

	sid := fmt.Sprintf("Function: %v", functionName)
	job := domain.NewJob2(sid, params.Input, nil)

	debugLog := func(msg string) {
		usecases.Log(&job, msg)
	}

	var resultOutput *Output = nil

	// one-time-called function with lot of closures
	perform := func(*domain.Job) error {
		output, err := f(params.Input, ExecutionMeta{
			Log: debugLog,
		})
		if err != nil {
			return err
		}

		if params.SaveOutput {
			usecases.SetOutputRaw(&job, *output)
		}

		resultOutput = output
		return nil
	}

	if err := usecases.RunAsWrappedFunction(&job, perform); err != nil {
		return nil, err
	}

	return resultOutput, nil
}
