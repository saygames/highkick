package http_intercom

import (
	"bytes"
	"context"
	"io"
	"net/http"
	"net/url"
	"time"

	"github.com/pkg/errors"
)

type HttpClient struct {
	cli      *http.Client
	host     string
	authFunc func(req *http.Request)
}

type HttpConnectionParams struct {
	Host     string
	Timeout  time.Duration
	AuthFunc func(req *http.Request)
}

func NewHttpClient(params HttpConnectionParams) *HttpClient {
	httpClient := &http.Client{
		Timeout: params.Timeout,
	}

	return &HttpClient{
		cli:      httpClient,
		host:     params.Host,
		authFunc: params.AuthFunc,
	}
}

func (c *HttpClient) sendData(ctx context.Context, method string, queryParams map[string]string, urlPath string, body []byte) (*Response, error) {
	req, err := http.NewRequestWithContext(ctx, method, c.host+urlPath, bytes.NewBuffer(body))
	if err != nil {
		return nil, errors.Wrap(err, "failed to create request")
	}

	if len(queryParams) != 0 {
		values := url.Values{}
		for key, value := range queryParams {
			values.Add(key, value)
		}
		rawQ := values.Encode()

		req.URL.RawQuery = rawQ
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")

	if c.authFunc != nil {
		c.authFunc(req)
	}

	resp, err := c.cli.Do(req)
	if err != nil {
		return nil, errors.Wrap(err, "failed to send request")
	}
	defer resp.Body.Close()

	result := Response{
		StatusCode: resp.StatusCode,
	}

	result.Body, err = io.ReadAll(resp.Body)
	if err != nil {

		return nil, errors.Wrap(err, "failed to read response body")
	}

	if result.StatusCode < http.StatusOK || result.StatusCode >= http.StatusBadRequest {

		return &Response{StatusCode: resp.StatusCode}, errors.Wrapf(err, "bad response status code: %v, '%v'", resp.StatusCode, string(result.Body))
	}

	return &result, nil
}
