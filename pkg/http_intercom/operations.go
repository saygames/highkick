package http_intercom

import (
	"context"
	"encoding/json"
	"fmt"

	"bitbucket.org/saygames/highkick/internal/domain"
	"github.com/pkg/errors"
)

func (c *HttpClient) RunJob(sid string, input domain.JSONDictionary) (int, error) {
	body, err := json.Marshal(JobRunRequest{
		SID:   sid,
		Input: input,
	})
	if err != nil {
		return 0, errors.Wrap(err, "failed to marshal job run request")
	}

	resp, err := c.sendData(context.Background(), "POST", nil, "/highkick/jobs/run", body)
	if err != nil {
		return 0, errors.Wrap(err, "failed to send job run request")
	}

	var jobInfo struct {
		Job domain.Job `json:"job"`
	}
	err = json.Unmarshal(resp.Body, &jobInfo)
	if err != nil {
		return 0, errors.Wrap(err, "failed to unmarshal job run response")
	}

	return jobInfo.Job.ID, nil
}

func (c *HttpClient) GetJobInfo(jobID int) (*domain.Job, error) {
	resp, err := c.sendData(context.Background(), "GET", nil, fmt.Sprintf("/highkick/jobs/show/%d", jobID), nil)
	if err != nil {
		return nil, errors.Wrap(err, "failed to send job info request")
	}

	var jobInfo struct {
		Job domain.Job `json:"job"`
	}
	err = json.Unmarshal(resp.Body, &jobInfo)
	if err != nil {
		return nil, errors.Wrap(err, "failed to unmarshal job info response")
	}

	return &jobInfo.Job, nil
}
