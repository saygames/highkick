package http_intercom

import (
	"fmt"
	"time"

	"bitbucket.org/saygames/highkick/internal/domain"
)

type JobIDResponse struct {
	JobID int `json:"id"`
}

type PollJobStatusParams struct {
	Limit           int
	IntervalSeconds float64
	DebugLog        func(string)
}

func (client HttpClient) PollJobStatus(jobID int, params PollJobStatusParams) (*domain.Job, error) {
	limit := params.Limit

	for {
		job, err := client.GetJobInfo(jobID)
		if err != nil {
			return nil, err
		}

		params.DebugLog(fmt.Sprintf("Highkick: Got: %+v", job))

		if job.Status == domain.JobStatuses.Completed {
			return job, nil
		}
		if job.Status == domain.JobStatuses.Failed {
			return job, fmt.Errorf("Job failed")
		}

		time.Sleep(time.Duration(params.IntervalSeconds) * time.Second)
		limit -= 1
		if limit < 0 {
			break
		}
	}

	return nil, fmt.Errorf("Limit of requests reached")
}
