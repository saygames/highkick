package http_intercom

import "bitbucket.org/saygames/highkick/internal/domain"

type JobRunRequest struct {
	SID   string
	Input domain.JSONDictionary
}

type Response struct {
	Body        []byte
	StatusCode  int
	ContentType string
}
