package jobs

import (
	"fmt"
	"strings"
	"time"

	"bitbucket.org/saygames/highkick/internal/domain"
)

type QueryBuilder_ForWorker struct {
	JobsToHandle domain.JobsToHandle
	WorkerSID    string
}

type QueryBuilder struct {
	ID         *int
	IDLessThan *int
	IDs        *[]int

	Query         *string
	IsRoot        *bool
	SubtreeOf     *domain.Job
	ChildrenOf    *domain.Job
	SID           *string
	JobSIDs       *[]string
	JobSIDsNot    *[]string
	SiblingsOf    *domain.Job
	Status        *domain.JobStatus
	Statuses      *[]domain.JobStatus
	RunWithWorker *string // simple exect where clause
	WorkerIDs     []int

	InputContains *string // uses JSON_CONTAINS

	ForWorker *QueryBuilder_ForWorker

	Since *time.Time
	Till  *time.Time

	OrderByIDDesc *bool
	OrderByIDAsc  *bool

	Page    *int
	PerPage *int
}

func (f QueryBuilder) Select() *[]string {
	return &[]string{
		"id", "sid", "path", "full_path", "input", "output", "status", "retries_left", "logs_count", "run_with_worker", "worker_id", "started_at", "finished_at", "created_at",
	}
}

func (f QueryBuilder) ForceIndex() *string {
	return nil
	// forceIndex := fmt.Sprintf("FORCE INDEX (PRIMARY, jobs_fullpath1_idx, jobs_fullpath2_idx, jobs_fullpath3_idx, jobs_fullpath4_idx, jobs_fullpath5_idx, jobs_fullpath6_idx, jobs_fullpath7_idx, jobs_fullpath8_idx, jobs_fullpath9_idx, jobs_fullpath10_idx, jobs_fullpath11_idx, jobs_fullpath12_idx, jobs_fullpath13_idx, jobs_fullpath14_idx, jobs_fullpath15_idx)")
	// return &forceIndex
}

func (f QueryBuilder) Join() *string {
	return nil
}

func (f QueryBuilder) Where() string {
	__escapeStringSlice := func(slice []string) []string {
		escaped := []string{}
		for _, v := range slice {
			escaped = append(escaped, fmt.Sprintf(`'%v'`, v))
		}
		return escaped
	}

	generateFullPathOptions := func(parentID int) []string {
		return []string{
			fmt.Sprintf("full_path1 = %v", parentID),
			fmt.Sprintf("full_path2 = %v", parentID),
			fmt.Sprintf("full_path3 = %v", parentID),
			fmt.Sprintf("full_path4 = %v", parentID),
			fmt.Sprintf("full_path5 = %v", parentID),
			fmt.Sprintf("full_path6 = %v", parentID),
			fmt.Sprintf("full_path7 = %v", parentID),
			fmt.Sprintf("full_path8 = %v", parentID),
			fmt.Sprintf("full_path9 = %v", parentID),
			fmt.Sprintf("full_path10 = %v", parentID),
			fmt.Sprintf("full_path11 = %v", parentID),
			fmt.Sprintf("full_path12 = %v", parentID),
			fmt.Sprintf("full_path13 = %v", parentID),
			fmt.Sprintf("full_path14 = %v", parentID),
			fmt.Sprintf("full_path15 = %v", parentID),
		}
	}

	generateFullPathOptionsToFindDirectChildren := func(job domain.Job) []string {
		result := []string{}
		N := 15
		n := len(job.FullPathWithoutZeros())
		for i, pathID := range job.FullPathWithoutZeros() {
			result = append(result, fmt.Sprintf("full_path%v = %v", (i+1), pathID))
		}
		if n < N {
			result = append(result, fmt.Sprintf("full_path%v = %v", (n+1), job.ID))
		}
		for i := n + 1; i < N; i++ {
			result = append(result, fmt.Sprintf("full_path%v = %v", (i+1), 0))
		}
		return result
	}

	// ACTUAL QUERY

	clauses := []string{"true"}

	if f.ID != nil {
		clauses = append(clauses, fmt.Sprintf("(id = %v)", *f.ID))
	}
	if f.IDLessThan != nil {
		clauses = append(clauses, fmt.Sprintf("(id < %v)", *f.IDLessThan))
	}

	if f.IDs != nil && len(*f.IDs) > 0 {
		escaped := []string{}
		for _, v := range *f.IDs {
			escaped = append(escaped, fmt.Sprintf(`%v`, v))
		}
		clauses = append(clauses, fmt.Sprintf("id IN (%v)", strings.Join(escaped, ", ")))
	}

	if f.IsRoot != nil {
		if *f.IsRoot == true {
			clauses = append(clauses, "(path = '')")
		} else {
			clauses = append(clauses, "(path != '')")
		}
	}

	if f.SubtreeOf != nil {
		root := *f.SubtreeOf
		// Old way via path and LIKE
		// {
		// 	clauses = append(clauses, fmt.Sprintf(
		// 		`(path LIKE "%v/%v/%%" OR path LIKE "%%/%v/%v/%%" OR path = "%v/%v" OR path LIKE "%v/%%" OR path LIKE "%%/%v/%%" OR path = "%v" OR id = %v)`,
		// 		root.Path, root.ID, root.Path, root.ID, root.Path, root.ID, root.ID, root.ID, root.ID, root.ID,
		// 	))
		// }
		// New way via JSON array and generated columns
		{
			options := generateFullPathOptions(root.ID)
			options = append(options, fmt.Sprintf("id = %v", root.ID))
			clauses = append(clauses, fmt.Sprintf(`( (%v) )`, strings.Join(options, ") OR (")))
		}
	}

	if f.ChildrenOf != nil {
		// New way via JSON array and generated columns
		{
			options := generateFullPathOptionsToFindDirectChildren(*f.ChildrenOf)
			clauses = append(clauses, fmt.Sprintf(`( (%v) )`, strings.Join(options, ") AND (")))
		}
	}

	if f.SiblingsOf != nil {
		root := *f.SiblingsOf
		clauses = append(clauses, fmt.Sprintf(
			`(path = "%v/%v" OR path = "%v")`,
			root.Path, root.ID, root.ID,
		))
	}

	if f.SID != nil {
		clauses = append(clauses, fmt.Sprintf("(sid = '%v')", *f.SID))
	}

	if f.Status != nil {
		clauses = append(clauses, fmt.Sprintf("(status = '%v')", *f.Status))
	}

	if f.Statuses != nil && len(*f.Statuses) > 0 {
		escaped := []string{}
		for _, v := range *f.Statuses {
			escaped = append(escaped, fmt.Sprintf(`"%v"`, v))
		}
		clauses = append(clauses, fmt.Sprintf("status IN (%v)", strings.Join(escaped, ", ")))
	}

	if f.Since != nil {
		clauses = append(clauses, fmt.Sprintf("created_at >= '%v'", (*f.Since).Format("2006-01-02")))
	}
	if f.Till != nil {
		clauses = append(clauses, fmt.Sprintf("created_at <= '%v'", (*f.Till).Format("2006-01-02")))
	}

	if f.JobSIDs != nil && len(*f.JobSIDs) > 0 {
		escaped := __escapeStringSlice(*f.JobSIDs)
		clauses = append(clauses, fmt.Sprintf("sid IN (%v)", strings.Join(escaped, ", ")))
	}

	if f.JobSIDsNot != nil && len(*f.JobSIDsNot) > 0 {
		escaped := __escapeStringSlice(*f.JobSIDsNot)
		clauses = append(clauses, fmt.Sprintf("sid NOT IN (%v)", strings.Join(escaped, ", ")))
	}

	if f.RunWithWorker != nil {
		clauses = append(clauses, fmt.Sprintf("run_with_worker = '%v'", *f.RunWithWorker))
	}

	if f.ForWorker != nil {
		jobSidsOnlyClause := "1=1"
		if escaped := __escapeStringSlice(f.ForWorker.JobsToHandle.Only); len(escaped) > 0 {
			jobSidsOnlyClause = fmt.Sprintf("sid IN (%v)", strings.Join(escaped, ", "))
		}
		jobSidsExceptClause := "1=1"
		if escaped := __escapeStringSlice(f.ForWorker.JobsToHandle.Except); len(escaped) > 0 {
			jobSidsOnlyClause = fmt.Sprintf("sid NOT IN (%v)", strings.Join(escaped, ", "))
		}
		clause1 := fmt.Sprintf("(%v AND %v AND run_with_worker = '')", jobSidsOnlyClause, jobSidsExceptClause)
		clause2 := "1=1"
		if f.ForWorker.WorkerSID != "" {
			clause2 = fmt.Sprintf("run_with_worker = '%v'", f.ForWorker.WorkerSID)
		}
		clauses = append(clauses, fmt.Sprintf("(%v OR %v)", clause1, clause2))
	}

	if f.Query != nil {
		clauses2 := []string{
			fmt.Sprintf("id = '%v'", *f.Query),
			fmt.Sprintf("sid LIKE '%%%v%%'", *f.Query),
		}
		clauses = append(clauses, fmt.Sprintf("(%v)", strings.Join(clauses2, " OR ")))
	}

	if f.InputContains != nil && *f.InputContains != "" {
		clauses = append(clauses, fmt.Sprintf("(JSON_CONTAINS(input, '%v', '$') = 1)", *f.InputContains))
	}

	if f.WorkerIDs != nil {
		if len(f.WorkerIDs) > 0 {
			escaped := []string{}
			for _, v := range f.WorkerIDs {
				escaped = append(escaped, fmt.Sprintf(`%v`, v))
			}
			clauses = append(clauses, fmt.Sprintf("worker_id IN (%v)", strings.Join(escaped, ", ")))
		} else {
			clauses = append(clauses, "1 = 2")
		}
	}

	return strings.Join(clauses, " AND ")
}

func (qb QueryBuilder) GroupBy() *[]string {
	return nil
}

func (qb QueryBuilder) OrderBy() *string {
	if qb.OrderByIDDesc != nil && *qb.OrderByIDDesc == true {
		q := "id DESC"
		return &q
	}
	if qb.OrderByIDAsc != nil && *qb.OrderByIDAsc == true {
		q := "id ASC"
		return &q
	}
	return nil
}

func (qb QueryBuilder) Pagination() *struct {
	Page    int
	PerPage int
} {
	if qb.Page != nil && qb.PerPage != nil {
		return &struct {
			Page    int
			PerPage int
		}{
			Page:    *qb.Page,
			PerPage: *qb.PerPage,
		}
	}
	return nil
}
