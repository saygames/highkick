package jobs

import (
	"encoding/json"
	"fmt"

	"gopkg.in/reform.v1"

	"bitbucket.org/saygames/highkick/internal/domain"
	repositoryLib "bitbucket.org/saygames/repository/pkg/mysql"
)

var Repo repositoryLib.Repository[domain.Job, *domain.Job]

func InitializeRepo(dbr *reform.DB) {
	pack := func(job *domain.Job) error {
		if jsonBytes, err := json.Marshal(job.FullPathWithZeros(15)); err == nil {
			job.FullPathJSONAsString = string(jsonBytes)
			return nil
		} else {
			return err
		}
	}

	unpack := func(job *domain.Job) error {
		fullPathJSONAsString := job.FullPathJSONAsString
		if fullPathJSONAsString == "" {
			fullPathJSONAsString = "[]"
		}
		if err := json.Unmarshal([]byte(fullPathJSONAsString), &job.FullPath); err != nil {
			return err
		}
		return nil
	}

	Repo = repositoryLib.Repository[domain.Job, *domain.Job]{
		DB:        dbr,
		View:      domain.JobTable,
		EnableLog: false,
		Pack:      &pack,
		Unpack:    &unpack,
	}

	fmt.Println("[Repo] [Jobs] Initialized")
}
