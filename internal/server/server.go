package server

import (
	"net/http"
	"net/url"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/markbates/pkger"

	"bitbucket.org/saygames/highkick/internal/job_logs"
	"bitbucket.org/saygames/highkick/internal/job_stats"
	"bitbucket.org/saygames/highkick/internal/jobs1"
	"bitbucket.org/saygames/highkick/internal/schedulers"
	"bitbucket.org/saygames/highkick/internal/server/middlewares"
	"bitbucket.org/saygames/highkick/internal/server/ws"
	"bitbucket.org/saygames/highkick/internal/workers"
)

type ServerParams struct {
	AuthMiddleware *gin.HandlerFunc
	ClientURL      string
}

func RunServer(engine *gin.Engine, params ServerParams) {
	engine.Use(middlewares.HandleError)

	// CLIENT

	{
		clientHandler := http.FileServer(pkger.Dir("/client/build"))
		urlPrefix := params.ClientURL
		if !strings.HasSuffix(urlPrefix, "/") {
			urlPrefix += "/"
		}
		engine.Any(urlPrefix+"*path", func(ctx *gin.Context) {
			urlWithoutPrefix, err := url.Parse(ctx.Param("path"))
			if err != nil {
				ctx.JSON(500, map[string]string{
					"error": err.Error(),
				})
				return
			}
			ctx.Request.URL = urlWithoutPrefix

			clientHandler.ServeHTTP(ctx.Writer, ctx.Request)
		})
	}

	// SERVER

	// unauthorized
	unauthorized := engine.Group("highkick")
	unauthorized.GET("/jobs/show/:job_id", jobs1.Show)
	unauthorized.GET("/ws", ws.HttpUpgadeHandler)
	// engine.Static("/highkick/client", "../client/build")

	// authorized
	authorized := engine.Group("highkick")
	if params.AuthMiddleware != nil {
		authorized.Use(*params.AuthMiddleware)
	}

	{
		g := authorized.Group("/highkick")
		g.GET("/hello", Hello)
	}

	{
		g := authorized.Group("/jobs")
		g.POST("/run", jobs1.Run)
		g.DELETE("/delete/:job_id", jobs1.Destroy)
		g.POST("/retry/:job_id/", jobs1.Retry)
		g.POST("/retry_failed_leaves/:job_id/", jobs1.RetryFailedLeaves)
		g.GET("/subtree/:job_id", jobs1.Subtree)
		g.GET("/input/:job_id", jobs1.GetInput)
		g.POST("/update/:job_id", jobs1.UpdateHandler)
		g.GET("/children/:job_id", jobs1.ChildrenHandler)
		g.POST("/last", jobs1.GetLastHandler)
	}

	{
		g := authorized.Group("/job_roots")
		g.GET("/index", jobs1.IndexRootsHandler)
		g.GET("/active", jobs1.ActiveRootsHandler)
	}

	{
		authorized.GET("/job_metas/index", jobs1.IndexJobMetas)
		authorized.GET("/job_logs/index_by_job_id/:job_id", job_logs.IndexByJobID)
		authorized.GET("/job_logs/index", job_logs.Index)
	}

	{
		authorized.GET("/job_stats/index", job_stats.Index)
	}

	{
		g := authorized.Group("/schedulers")
		g.GET("/index", schedulers.Index)
		g.GET("/show/:id", schedulers.Show)
		g.POST("/create", schedulers.CreateUpdate)
		g.POST("/update/:id", schedulers.CreateUpdate)
		g.DELETE("/destroy/:id", schedulers.Destroy)
	}

	{
		g := authorized.Group("/workers")
		g.GET("/index", workers.Index)
		g.GET("/show/:id", workers.Show)
		g.POST("/update/:id", workers.CreateUpdate)
		g.DELETE("/destroy/:id", workers.Destroy)
	}
}
