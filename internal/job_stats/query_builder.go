package job_stats

import (
	"fmt"
	"strings"
	"time"
)

type QueryBuilder struct {
	Since   *time.Time
	Till    *time.Time
	Page    *int
	PerPage *int
}

func (f QueryBuilder) Select() *[]string {
	return &[]string{
		"sid",
		"DATE(created_at) as `date`",
		"SUM(case when status IN ('scheduled', 'initial', 'processing') then 1 else 0 end) as running",
		"SUM(case when status IN ('failed') then 1 else 0 end) as failed",
		"SUM(case when status IN ('completed') then 1 else 0 end) as completed",
	}
}

func (f QueryBuilder) ForceIndex() *string {
	return nil
}

func (f QueryBuilder) Join() *string {
	return nil
}

func (f QueryBuilder) Where() string {
	clauses := []string{"true"}

	if f.Since != nil {
		clauses = append(clauses, fmt.Sprintf("created_at >= '%v 00:00:00'", (*f.Since).Format("2006-01-02")))
	}
	if f.Till != nil {
		clauses = append(clauses, fmt.Sprintf("created_at <= '%v 23:59:59'", (*f.Till).Format("2006-01-02")))
	}

	return strings.Join(clauses, " AND ")
}

func (qb QueryBuilder) GroupBy() *[]string {
	return &[]string{"sid", "`date`"}
}

func (qb QueryBuilder) OrderBy() *string {
	return nil
}

func (qb QueryBuilder) Pagination() *struct {
	Page    int
	PerPage int
} {
	if qb.Page != nil && qb.PerPage != nil {
		return &struct {
			Page    int
			PerPage int
		}{
			Page:    *qb.Page,
			PerPage: *qb.PerPage,
		}
	}
	return nil
}
