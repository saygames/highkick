package job_stats

import (
	"fmt"

	repositoryLib "bitbucket.org/saygames/repository/pkg/mysql"
	"gopkg.in/reform.v1"
)

var Repo repositoryLib.Repository[JobStat, *JobStat]

func InitializeRepo(dbr *reform.DB) {
	Repo = repositoryLib.Repository[JobStat, *JobStat]{
		DB:        dbr,
		View:      JobStatView,
		EnableLog: true,
	}
	fmt.Println("[Repo] [JobStats] Initialized")
}
