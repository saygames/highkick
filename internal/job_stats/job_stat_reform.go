// Code generated by gopkg.in/reform.v1. DO NOT EDIT.

package job_stats

import (
	"fmt"
	"strings"

	"gopkg.in/reform.v1"
	"gopkg.in/reform.v1/parse"
)

type jobStatViewType struct {
	s parse.StructInfo
	z []interface{}
}

// Schema returns a schema name in SQL database ("").
func (v *jobStatViewType) Schema() string {
	return v.s.SQLSchema
}

// Name returns a view or table name in SQL database ("jobs").
func (v *jobStatViewType) Name() string {
	return v.s.SQLName
}

// Columns returns a new slice of column names for that view or table in SQL database.
func (v *jobStatViewType) Columns() []string {
	return []string{
		"sid",
		"date",
		"running",
		"failed",
		"completed",
	}
}

// NewStruct makes a new struct for that view or table.
func (v *jobStatViewType) NewStruct() reform.Struct {
	return new(JobStat)
}

// JobStatView represents jobs view or table in SQL database.
var JobStatView = &jobStatViewType{
	s: parse.StructInfo{
		Type:    "JobStat",
		SQLName: "jobs",
		Fields: []parse.FieldInfo{
			{Name: "SID", Type: "string", Column: "sid"},
			{Name: "Date", Type: "time.Time", Column: "date"},
			{Name: "Running", Type: "int", Column: "running"},
			{Name: "Failed", Type: "int", Column: "failed"},
			{Name: "Completed", Type: "int", Column: "completed"},
		},
		PKFieldIndex: -1,
	},
	z: new(JobStat).Values(),
}

// String returns a string representation of this struct or record.
func (s JobStat) String() string {
	res := make([]string, 5)
	res[0] = "SID: " + reform.Inspect(s.SID, true)
	res[1] = "Date: " + reform.Inspect(s.Date, true)
	res[2] = "Running: " + reform.Inspect(s.Running, true)
	res[3] = "Failed: " + reform.Inspect(s.Failed, true)
	res[4] = "Completed: " + reform.Inspect(s.Completed, true)
	return strings.Join(res, ", ")
}

// Values returns a slice of struct or record field values.
// Returned interface{} values are never untyped nils.
func (s *JobStat) Values() []interface{} {
	return []interface{}{
		s.SID,
		s.Date,
		s.Running,
		s.Failed,
		s.Completed,
	}
}

// Pointers returns a slice of pointers to struct or record fields.
// Returned interface{} values are never untyped nils.
func (s *JobStat) Pointers() []interface{} {
	return []interface{}{
		&s.SID,
		&s.Date,
		&s.Running,
		&s.Failed,
		&s.Completed,
	}
}

// View returns View object for that struct.
func (s *JobStat) View() reform.View {
	return JobStatView
}

// check interfaces
var (
	_ reform.View   = JobStatView
	_ reform.Struct = (*JobStat)(nil)
	_ fmt.Stringer  = (*JobStat)(nil)
)

func init() {
	parse.AssertUpToDate(&JobStatView.s, new(JobStat))
}
