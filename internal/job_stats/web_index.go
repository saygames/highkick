package job_stats

import (
	"net/http"

	"encoding/json"

	"github.com/gin-gonic/gin"
)

// Index is Index
func Index(ctx *gin.Context) {
	qb := QueryBuilder{}
	if err := json.Unmarshal([]byte(ctx.Query("filters")), &qb); err != nil {
		panic(err)
	}

	items, err := Repo.Select(qb)
	if err != nil {
		panic(err)
	}

	ctx.JSON(http.StatusOK, struct {
		Items []JobStat `json:"items"`
	}{
		Items: items,
	})
}
