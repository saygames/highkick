package job_stats

import "time"

//go:generate reform
//reform:jobs
type JobStat struct {
	SID       string    `reform:"sid"`
	Date      time.Time `reform:"date"`
	Running   int       `reform:"running"`
	Failed    int       `reform:"failed"`
	Completed int       `reform:"completed"`
}
