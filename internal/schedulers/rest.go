package schedulers

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin/binding"

	"github.com/gin-gonic/gin"
)

// Destroy .
func Destroy(c *gin.Context) {
	idStr := c.Param("id")
	idInt64, err := strconv.ParseInt(idStr, 10, 64)
	if err != nil {
		panic(err)
	}

	scheduler, err := Repo.SelectOne(int(idInt64))
	if err != nil {
		panic(err)
	}

	if err := Repo.Destroy(scheduler); err != nil {
		panic(err)
	}

	c.JSON(http.StatusOK, struct{}{})
}

func Index(ctx *gin.Context) {
	schedulers, err := Repo.Select(QueryBuilder{})
	if err != nil {
		panic(err)
	}

	ctx.JSON(http.StatusOK, schedulers)
}

func Show(ctx *gin.Context) {
	idStr := ctx.Param("id")
	idInt64, err := strconv.ParseInt(idStr, 10, 64)
	if err != nil {
		panic(err)
	}

	scheduler, err := Repo.SelectOne(int(idInt64))
	if err != nil {
		panic(err)
	}

	ctx.JSON(http.StatusOK, *scheduler)
}

func CreateUpdate(c *gin.Context) {
	var scheduler Scheduler
	err := c.ShouldBindBodyWith(&scheduler, binding.JSON)
	if err != nil {
		panic(err)
	}

	if err := Repo.Save(&scheduler); err != nil {
		panic(err)
	}

	c.JSON(http.StatusOK, scheduler)
}
