package schedulers

import (
	"encoding/json"
	"time"

	"bitbucket.org/saygames/highkick/internal/domain"
	"bitbucket.org/saygames/highkick/internal/lib"
)

type SchedulerType string

var SchedulerTypes = struct {
	Timer     SchedulerType
	ExactTime SchedulerType
	Cron      SchedulerType
}{
	Timer:     "timer",
	ExactTime: "exact_time",
	Cron:      "cron",
}

//go:generate reform
//reform:highkick_schedulers
type Scheduler struct {
	ID       int    `reform:"id,pk"`
	JobSID   string `reform:"job_sid"`
	JobInput string `reform:"job_input"`

	SchedulerType SchedulerType `reform:"scheduler_type"`
	// SchedulerType=Timer
	RunEverySeconds int `reform:"run_every_seconds"`
	// SchedulerType=Exact time
	ExactTimes lib.StringList `reform:"exact_times"`
	// SchedulerType=Cron
	Cron string `reform:"cron"`

	Stopped   bool      `reform:"stopped"`
	UpdatedAt time.Time `reform:"updated_at"`

	WorkerID int // Keeps the reference to assigned worker

	LastRunAt *time.Time `reform:"last_run_at"`
	LastError string     `reform:"last_error"`
}

func (s *Scheduler) BeforeInsert() error {
	s.UpdatedAt = time.Now()
	return nil
}

func (s *Scheduler) BeforeUpdate() error {
	s.UpdatedAt = time.Now()
	return nil
}

// GetInput is getter for Input
func (s *Scheduler) GetJobInput() domain.JSONDictionary {
	var dict domain.JSONDictionary
	_ = json.Unmarshal([]byte(s.JobInput), &dict)
	return dict
}

func (a Scheduler) Equal(b Scheduler) bool {
	return (a.JobSID == b.JobSID &&
		a.JobInput == b.JobInput &&
		a.SchedulerType == b.SchedulerType &&
		a.RunEverySeconds == b.RunEverySeconds &&
		a.ExactTimes.Equal(b.ExactTimes) &&
		a.Cron == b.Cron)
}
