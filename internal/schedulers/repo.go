package schedulers

import (
	"fmt"

	"gopkg.in/reform.v1"

	repositoryLib "bitbucket.org/saygames/repository/pkg/mysql"
)

var Repo repositoryLib.Repository[Scheduler, *Scheduler]

func InitializeRepo(dbr *reform.DB) {
	Repo = repositoryLib.Repository[Scheduler, *Scheduler]{
		DB:        dbr,
		View:      SchedulerTable,
		EnableLog: false,
	}
	fmt.Println("[Repo] [Schedulers] Initialized")
}
