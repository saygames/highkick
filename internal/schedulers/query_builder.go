package schedulers

import (
	"fmt"
	"strings"
)

type QueryBuilder struct {
	ID            *int
	JobSIDs       *[]string
	JobSIDsNot    *[]string
	SchedulerType *SchedulerType
	Page          *int
	PerPage       *int
}

func (f QueryBuilder) Select() *[]string {
	return nil
}

func (f QueryBuilder) ForceIndex() *string {
	return nil
}

func (f QueryBuilder) Join() *string {
	return nil
}

func (f QueryBuilder) Where() string {
	clauses := []string{"true"}

	if f.ID != nil {
		clauses = append(clauses, fmt.Sprintf("(id = %v)", *f.ID))
	}
	if f.JobSIDs != nil && len(*f.JobSIDs) > 0 {
		escaped := []string{}
		for _, v := range *f.JobSIDs {
			escaped = append(escaped, fmt.Sprintf(`"%v"`, v))
		}
		clauses = append(clauses, fmt.Sprintf("job_sid IN (%v)", strings.Join(escaped, ", ")))
	}
	if f.JobSIDsNot != nil && len(*f.JobSIDsNot) > 0 {
		escaped := []string{}
		for _, v := range *f.JobSIDsNot {
			escaped = append(escaped, fmt.Sprintf(`"%v"`, v))
		}
		clauses = append(clauses, fmt.Sprintf("job_sid NOT IN (%v)", strings.Join(escaped, ", ")))
	}
	if f.SchedulerType != nil {
		clauses = append(clauses, fmt.Sprintf("scheduler_type = '%v'", *f.SchedulerType))
	}

	return strings.Join(clauses, " AND ")
}

func (qb QueryBuilder) GroupBy() *[]string {
	return nil
}

func (qb QueryBuilder) OrderBy() *string {
	return nil
}

func (qb QueryBuilder) Pagination() *struct {
	Page    int
	PerPage int
} {
	if qb.Page != nil && qb.PerPage != nil {
		return &struct {
			Page    int
			PerPage int
		}{
			Page:    *qb.Page,
			PerPage: *qb.PerPage,
		}
	}
	return nil
}
