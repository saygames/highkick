package usecases

import (
	"fmt"
	"os"

	"bitbucket.org/saygames/highkick/internal/domain"
	"github.com/tidwall/gjson"
)

const HIGHKICK_STOP_WORKER = "HIGHKICK/STOP_WORKER"

func init() {
	inputJSONSchema := `{
		"type": "object",
		"properties": {
			"WorkerID": { "type": "number" }
		},
		"required": ["WorkerID"]
	}`
	Register(domain.JobMeta{
		SID:             HIGHKICK_STOP_WORKER,
		Title:           HIGHKICK_STOP_WORKER,
		Perform:         HighkickStopWorkerJob,
		InputJSONSchema: &inputJSONSchema,
	})
}

func HighkickStopWorkerJob(job *domain.Job) error {
	for _, key := range []string{"WorkerID"} {
		if !gjson.Get(*job.Input, key).Exists() {
			return fmt.Errorf("%v is required", key)
		}
	}
	workerID := int(gjson.Get(*job.Input, "WorkerID").Int())

	// Double check
	if job.WorkerID != workerID {
		return fmt.Errorf("This job should not run on this worker. Alarm!!!")
	}

	// How to stop the worker?

	// Strategy A
	{
		os.Exit(137)
	}

	return nil
}

func ShallRunJobOnThisWorker(job domain.Job, workerID int) (*bool, error) {
	if job.Sid != HIGHKICK_STOP_WORKER {
		return nil, fmt.Errorf("[GetStopWorkerJobWorkerIDParameter] This job %v is not %v", job.ID, HIGHKICK_STOP_WORKER)
	}
	for _, key := range []string{"WorkerID"} {
		if !gjson.Get(*job.Input, key).Exists() {
			return nil, fmt.Errorf("[GetStopWorkerJobWorkerIDParameter] %v is required", key)
		}
	}
	workerIDFromJobParams := int(gjson.Get(*job.Input, "WorkerID").Int())

	truly := true
	falsy := false

	if workerIDFromJobParams != workerID {
		return &falsy, nil
	}

	// Match
	return &truly, nil
}
