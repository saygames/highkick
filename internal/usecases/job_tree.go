package usecases

import (
	"bitbucket.org/saygames/highkick/internal/domain"
	"bitbucket.org/saygames/highkick/internal/jobs"
)

// GetJobTreeStatus .
func GetJobTreeStatus(job domain.Job) (*domain.JobStatus, error) {
	jobs, err := jobs.Repo.Select(jobs.QueryBuilder{
		SubtreeOf: &job,
	})
	if err != nil {
		return nil, err
	}

	anyProcessing := false
	anyFailed := false
	allCompleted := true
	for _, j := range jobs {
		switch j.Status {
		case domain.JobStatuses.Processing:
			anyProcessing = true
			allCompleted = false
		case domain.JobStatuses.Failed:
			anyFailed = true
			allCompleted = false
		case domain.JobStatuses.Initial:
			allCompleted = false
		}
	}
	switch {
	case anyProcessing:
		return &domain.JobStatuses.Processing, nil
	case anyFailed:
		return &domain.JobStatuses.Failed, nil
	case allCompleted:
		return &domain.JobStatuses.Completed, nil
	default:
		return &domain.JobStatuses.Initial, nil
	}
}

func GetJobAndItsTreeStatus(job domain.Job) (*domain.JobStatus, error) {
	treeStatus, err := GetJobTreeStatus(job)
	if err != nil {
		return nil, err
	}

	if job.Status == domain.JobStatuses.Processing || *treeStatus == domain.JobStatuses.Processing {
		return &domain.JobStatuses.Processing, nil
	}

	if job.Status == domain.JobStatuses.Failed || *treeStatus == domain.JobStatuses.Failed {
		return &domain.JobStatuses.Failed, nil
	}

	return &job.Status, nil
}

// GetSiblingsDetailedStatus .
func GetSiblingsDetailedStatus(job *domain.Job) (*map[domain.JobStatus]int, error) {
	result := map[domain.JobStatus]int{}

	siblings, err := jobs.Repo.Select(jobs.QueryBuilder{
		SiblingsOf: job,
	})
	if err != nil {
		return nil, err
	}

	for _, sibling := range siblings {
		status, err := GetJobAndItsTreeStatus(sibling)
		if err != nil {
			return nil, err
		}
		if _, exists := result[*status]; exists == false {
			result[*status] = 0
		}
		result[*status] += 1
	}

	return &result, nil
}

type ChildrenStat struct {
	RootID           int
	ChildrenStatuses map[domain.JobStatus]int
}

func GetChildrenStat(job *domain.Job) (*ChildrenStat, error) {
	result := ChildrenStat{
		RootID:           job.ID,
		ChildrenStatuses: map[domain.JobStatus]int{},
	}

	children, err := jobs.Repo.Select(jobs.QueryBuilder{
		SubtreeOf: job,
	})
	if err != nil {
		return nil, err
	}

	for _, child := range children {
		result.ChildrenStatuses[child.Status] += 1
	}

	return &result, nil
}
