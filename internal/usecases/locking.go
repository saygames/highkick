package usecases

import (
	"sync"

	"bitbucket.org/saygames/highkick/internal/domain"
)

var mus = sync.Map{}

// Allows to lock job's SID so only one worker is running at each moment
//
func Lock(job domain.Job) {
	muInt, _ := mus.LoadOrStore(job.Sid, &sync.Mutex{})
	mu := muInt.(*sync.Mutex)
	mu.Lock()
}

func Unlock(job domain.Job) {
	muInt, _ := mus.LoadOrStore(job.Sid, &sync.Mutex{})
	mu := muInt.(*sync.Mutex)
	mu.Unlock()
}
