package usecases

import (
	"fmt"
	"sync"
	"time"

	"bitbucket.org/saygames/highkick/internal/domain"
	"bitbucket.org/saygames/highkick/internal/jobs"
	"bitbucket.org/saygames/highkick/internal/schedulers"
	"bitbucket.org/saygames/highkick/internal/workers"

	"github.com/go-co-op/gocron"
)

var DEBUG_LOG = true

type SchedulerMeta struct {
	StopChan            chan bool
	LastRun             *domain.Job
	Scheduler           schedulers.Scheduler
	StopGocronScheduler func()
}

type SchedulerMetas struct {
	data map[int]SchedulerMeta
	mu   *sync.RWMutex
}

func (sm SchedulerMetas) Get(schedulerID int) *SchedulerMeta {
	sm.mu.RLock()
	defer sm.mu.RUnlock()

	v, ok := sm.data[schedulerID]
	if !ok {
		return nil
	}

	return &v
}

func (sm SchedulerMetas) Set(schedulerID int, schedulerMeta SchedulerMeta) {
	sm.mu.Lock()
	defer sm.mu.Unlock()

	sm.data[schedulerID] = schedulerMeta
}

func (sm SchedulerMetas) Delete(schedulerID int) {
	sm.mu.Lock()
	defer sm.mu.Unlock()

	delete(sm.data, schedulerID)
}

func (sm SchedulerMetas) Snapshot() map[int]SchedulerMeta {
	sm.mu.RLock()
	defer sm.mu.RUnlock()

	clone := sm.data
	return clone
}

var schedulerMetas = SchedulerMetas{
	data: map[int]SchedulerMeta{},
	mu:   &sync.RWMutex{},
}

type RunSchedulersParams struct {
	JobsToHandle        domain.JobsToHandle
	DebugLog            func(string)
	PollIntervalSeconds int
}

func RunSchedulers(worker workers.Worker, params RunSchedulersParams) {
	fmt.Printf("[HIGHKICK] Worker %v: schedulers started\n", worker.ID)

	go func() {
		// Loop forever (until expicit break)
		every := time.Duration(params.PollIntervalSeconds) * time.Second
		if params.PollIntervalSeconds == 0 {
			every = 60 * time.Second
		}

		for {
			// Execute logic and return if the loop should be breaked or not
			shouldBreak := func() bool {
				defer func() {
					if r := recover(); r != nil {
						params.DebugLog(fmt.Sprintf("[RunSchedulers/RECOVER] %v", r))
					}
				}()

				worker, err := workers.Repo.SelectOne(worker.ID)
				if err != nil {
					params.DebugLog(fmt.Sprintf("[RunSchedulers/ERROR]", err))
					return false
				}
				if worker == nil {
					params.DebugLog(fmt.Sprintf("[RunSchedulers] No worker %v found\n", worker.ID))
					return true // should break!
				}
				if worker.Stopped {
					return true // should break!
				}

				prevSchedulerIDs := []int{}
				for schedulerID := range schedulerMetas.Snapshot() {
					prevSchedulerIDs = append(prevSchedulerIDs, schedulerID)
				}

				schedulers, err := schedulers.Repo.Select(schedulers.QueryBuilder{
					JobSIDs:    &params.JobsToHandle.Only,
					JobSIDsNot: &params.JobsToHandle.Except,
				})
				if err != nil {
					params.DebugLog(fmt.Sprintf("[HIGHKICK] Loading schedulers: %+v\n", err))
					return false
				}

				currentSchedulerIDs := map[int]bool{}
				for _, scheduler := range schedulers {
					scheduler.WorkerID = worker.ID // will be passed thru into runScheduler

					currentSchedulerIDs[scheduler.ID] = true
					if scheduler.Stopped {
						stopScheduler(scheduler.ID)
						continue
					}
					schedulerMeta := schedulerMetas.Get(scheduler.ID)
					if schedulerMeta != nil {
						// model was changed
						if !schedulerMeta.Scheduler.Equal(scheduler) {
							stopScheduler(scheduler.ID)
							startScheduler(scheduler)
						}
					} else {
						startScheduler(scheduler)
					}
				}

				for _, prevSchedulerID := range prevSchedulerIDs {
					// model was removed
					if _, ok := currentSchedulerIDs[prevSchedulerID]; !ok {
						stopScheduler(prevSchedulerID)
					}
				}

				return false
			}()
			if shouldBreak {
				break
			}

			time.Sleep(every)
		}

		fmt.Printf("[HIGHKICK] Worker %v: schedulers stopped\n", worker.ID)
	}()
}

func stopScheduler(schedulerID int) {
	if DEBUG_LOG {
		fmt.Printf("[Highkick] [Schedulers] Stopping %v [ENTER]\n", schedulerID)
	}

	schedulerMeta := schedulerMetas.Get(schedulerID)
	if schedulerMeta != nil {
		if DEBUG_LOG {
			fmt.Printf("[Highkick] [Schedulers] scheduler Meta = %v\n", schedulerMeta)
		}

		// Handle gocron
		if schedulerMeta.StopGocronScheduler != nil {
			if DEBUG_LOG {
				fmt.Printf("[Highkick] [Schedulers] gocron stopped [BEFORE]\n")
			}
			schedulerMeta.StopGocronScheduler()
			if DEBUG_LOG {
				fmt.Printf("[Highkick] [Schedulers] gocron stopped [AFTER]\n")
			}
		}

		// Handle stopChan
		schedulerMeta.StopChan <- true
		close(schedulerMeta.StopChan)

	}
	schedulerMetas.Delete(schedulerID)
	if DEBUG_LOG {
		fmt.Printf("[Highkick] [Schedulers] Stopping %v [EXIT]\n", schedulerID)
	}
}

func ExecuteScheduler(scheduler schedulers.Scheduler) error {
	job := domain.NewJob(scheduler.JobSID, scheduler.GetJobInput(), nil)
	job.WorkerID = scheduler.WorkerID
	job.CreatedAt = time.Now() // dirty

	// Update schedule meta
	{
		if schedulerMeta := schedulerMetas.Get(scheduler.ID); schedulerMeta != nil {
			schedulerMeta.LastRun = &job
			schedulerMetas.Set(scheduler.ID, *schedulerMeta)
		} else {
			fmt.Printf("[Highkick] [Schedulers] Can not update scheduler meta for %+v\n", scheduler)
		}
	}

	// Execute
	var executionErr error
	{
		switch scheduler.SchedulerType {
		case schedulers.SchedulerTypes.Timer:
			{
				_, executionErr = RunSync(job)
			}
		case schedulers.SchedulerTypes.ExactTime, schedulers.SchedulerTypes.Cron:
			{
				// TODO: we loose real execution status here, need workaround
				_, executionErr = RunAsync(job)
			}
		}
	}

	// Store all the details to database
	now := time.Now()
	scheduler.LastRunAt = &now
	if executionErr != nil {
		scheduler.LastError = fmt.Sprintf("%+v", executionErr)
	} else {
		scheduler.LastError = ""
	}
	if err := schedulers.Repo.DB.UpdateColumns(&scheduler, "last_run_at", "last_error"); err != nil {
		fmt.Printf("[Highkick] [Schedulers] Run failed with %v\n", err)
		return err
	}

	return executionErr
}

func startScheduler(scheduler schedulers.Scheduler) {
	fmt.Printf("[Highkick] [Schedulers] Starting %v\n", scheduler)

	stopChan := make(chan bool)

	lastRun := domain.Job{}
	{
		// Get last 10 of roots and choose with same input. Clumzy
		truly := true
		page := 1
		limit := 10
		if candidates, err := jobs.Repo.Select(jobs.QueryBuilder{
			SID:           &scheduler.JobSID,
			IsRoot:        &truly,
			OrderByIDDesc: &truly,
			Page:          &page,
			PerPage:       &limit,
		}); err == nil {
			for _, candidate := range candidates {
				if candidate.GetInput().Equal(scheduler.GetJobInput()) {
					lastRun = candidate
					break
				}
			}
		} else {
			fmt.Errorf("[Highkick] [Schedulers] Can not get last run. %v", err)
		}
	}

	// Init scheduler meta
	{
		schedulerMeta := SchedulerMeta{
			StopChan:  stopChan,
			LastRun:   nil,
			Scheduler: scheduler,
		}
		if lastRun.ID != 0 {
			schedulerMeta.LastRun = &lastRun
		}
		schedulerMetas.Set(scheduler.ID, schedulerMeta)
	}

	switch scheduler.SchedulerType {
	case schedulers.SchedulerTypes.Timer:
		{
			schedulerMeta := schedulerMetas.Get(scheduler.ID)
			ticker := time.NewTicker(time.Duration(scheduler.RunEverySeconds) * time.Second)
			go func() {
				if DEBUG_LOG {
					fmt.Printf("[Highkick] [Schedulers] Running Timer %v\n", scheduler)
				}
				for {
					select {
					case <-schedulerMeta.StopChan:
						if DEBUG_LOG {
							fmt.Printf("[Highkick] [Schedulers] Stopping %v\n 111", scheduler)
						}
						return
					case <-ticker.C:
						ExecuteScheduler(scheduler)
					}
				}
			}()
		}
	case schedulers.SchedulerTypes.ExactTime:
		{
			if DEBUG_LOG {
				fmt.Printf("[Highkick] [Schedulers] Running ExactTime %v\n", scheduler)
			}
			schedulerMeta := schedulerMetas.Get(scheduler.ID)
			ticker := time.NewTicker(27 * time.Second)
			go func() {
				for {
					select {
					case <-schedulerMeta.StopChan:
						if DEBUG_LOG {
							fmt.Printf("[Highkick] [Schedulers] Stopping %v\n 444", scheduler)
						}
						return
					case <-ticker.C:
						{
							if schedulerMeta := schedulerMetas.Get(scheduler.ID); schedulerMeta != nil {
								// Should run few times per minute
								lastRunAt := time.Time{}
								if schedulerMeta.LastRun != nil {
									lastRunAt = schedulerMeta.LastRun.CreatedAt
								}
								nowHHMM := time.Now().Format("15:04") // trigger
								triggeredHHMM := ""
								for _, candidate := range scheduler.ExactTimes {
									if candidate == nowHHMM {
										triggeredHHMM = candidate
										break
									} else if candidate > nowHHMM {
										break // assuming ExaxtTimes is sorted asc
									}
								}
								if triggeredHHMM != "" {
									if !lastRunAt.IsZero() && lastRunAt.Format("15:04") == triggeredHHMM && lastRunAt.Format("2006-01-02") == time.Now().Format("2006-01-02") {
										// double run. skip
									} else {
										ExecuteScheduler(scheduler)
									}
								}
							} else {
								fmt.Printf("[Highkick] [Schedulers] [Error] No meta found for a running exact-time scheduler !!!\n")
							}
						}
					}
				}
			}()
		}
	case schedulers.SchedulerTypes.Cron:
		{
			if DEBUG_LOG {
				fmt.Printf("[Highkick] [Schedulers] Running Cron %v\n", scheduler)
			}
			schedulerMeta := schedulerMetas.Get(scheduler.ID)

			// Nothing to do with stopChan
			go func() {
				<-schedulerMeta.StopChan
			}()

			// Run gocron
			// SingletonMode prevents a new job from starting if the prior job has not yet completed its run
			gocronScheduler := gocron.NewScheduler(time.UTC).SingletonMode()
			gocronScheduler.Cron(scheduler.Cron).Do(func() {
				ExecuteScheduler(scheduler)
			})

			stopGocronScheduler := func() {
				fmt.Printf("[Highkick] [Schedulers] stopGocronScheduler invoked\n")
				// It waits for all running jobs to finish before returning, so it is safe to assume that running jobs will finish when calling this.
				gocronScheduler.Stop()
			}

			schedulerMeta.StopGocronScheduler = stopGocronScheduler
			schedulerMetas.Set(scheduler.ID, *schedulerMeta) // save pointer to gocron in meta

			if DEBUG_LOG {
				fmt.Printf("[Highkick] [Schedulers] gocron started [BEFORE]\n")
			}
			gocronScheduler.StartAsync()
			if DEBUG_LOG {
				fmt.Printf("[Highkick] [Schedulers] gocron started [AFTER]\n")
			}
		}
	}
}
