package usecases

import (
	"strings"
	"sync"

	"bitbucket.org/saygames/highkick/internal/domain"
)

var jobMetas = map[string]domain.JobMeta{}
var mu sync.RWMutex

// Register registers worker for specified jobType
func Register(jobMeta domain.JobMeta) {
	if jobMeta.InputJSONSchema != nil {
		sanitized := strings.ReplaceAll(strings.ReplaceAll(*jobMeta.InputJSONSchema, "\t", ""), "\n", "")
		jobMeta.InputJSONSchema = &sanitized
	}

	mu.Lock()
	defer mu.Unlock()

	jobMetas[jobMeta.SID] = jobMeta

	// fmt.Printf("[Highkick] Job %v registered\n", jobMeta.SID)
}

// UnregisterAll unregisters all jobs
func UnregisterAll() {
	mu.Lock()
	defer mu.Unlock()

	jobMetas = map[string]domain.JobMeta{}
}

func GetJobs() []domain.JobMeta {
	mu.RLock()
	defer mu.RUnlock()

	result := []domain.JobMeta{}
	for _, j := range jobMetas {
		result = append(result, j)
	}
	return result
}
