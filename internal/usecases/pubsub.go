package usecases

import (
	"encoding/json"
	"fmt"

	"bitbucket.org/saygames/highkick/internal/domain"
	"bitbucket.org/saygames/highkick/internal/jobs"
	"bitbucket.org/saygames/highkick/internal/server/ws"
	"github.com/qw4n7y/gopubsub"
)

var JobsPubSub *gopubsub.Hub

func init() {
	JobsPubSub = gopubsub.NewHub()
}

var shouldBroadcastViaWS = true

func SetupJobUpdateBroadcasting(boardcastViaWS bool) {
	shouldBroadcastViaWS = boardcastViaWS
}

func BroadcastJobUpdate(job domain.Job, err error) {
	if shouldBroadcastViaWS {
		// WebSockets
		BroadcastJobUpdateViaWS(job)
	}

	// In-process pubsub, callbacks
	JobsPubSub.Publish(domain.PubSubMessage{
		Job:   job,
		Error: err,
	})
}

func BroadcastJobUpdateViaWS(job domain.Job) {
	root, err := jobs.Repo.SelectOne(job.GetRootID())
	if err != nil {
		fmt.Println("[BroadcastJobUpdateViaWS]", err)
		return
	}

	treeStatus, err := GetJobTreeStatus(*root)
	if err != nil {
		fmt.Println("[BroadcastJobUpdateViaWS]", err)
		return
	}

	root.TreeStatus = treeStatus

	rootJobJSON, _ := json.Marshal(root)
	message := ws.Message{
		Type:    "update",
		Payload: json.RawMessage(fmt.Sprintf(`{"job": %v}`, string(rootJobJSON))),
	}
	ws.TheHub.BroadcastToChannel("jobs", &message)
}
