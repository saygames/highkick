package usecases

import (
	"fmt"
	"os"
	"time"

	"bitbucket.org/saygames/highkick/internal/workers"
)

func RegisterAndRunWorker(worker workers.Worker, params WorkerParams) (*workers.Worker, error) {
	if err := workers.Repo.Save(&worker); err != nil {
		return nil, err
	}

	RunWorker(worker, params)

	return &worker, nil
}

// Timer to delete stopped workers with 0 running jobs
// Running callback
//
func RunWorkerMonitor(workerID int, onDestroy func(), debugLog func(string)) {
	go func() {
		every := 30 * time.Second

		for {
			worker, err := workers.Repo.SelectOne(workerID)
			if err != nil {
				debugLog(fmt.Sprintf("[WorkerMonitor] %v\n", err))
			} else if worker == nil {
				debugLog(fmt.Sprintf("[WorkerMonitor] No worker ID = %v found\n", workerID))
			} else {
				if worker.Killed {
					os.Exit(137)
				}
				if worker.Stopped {
					if worker.RunningJobsCount == 0 {
						// STOP
						if err := workers.Repo.Destroy(worker); err != nil {
							debugLog(fmt.Sprintf("[WorkerMonitor] %v\n", err))
						}
						debugLog(fmt.Sprintf("[WorkerMonitor] Worker %v destroyed\n", workerID))
						onDestroy()
					}
				}
				if err := workers.TrachHealthcheckTimestamp(worker.ID); err != nil {
					debugLog(fmt.Sprintf("[WorkerMonitor] TrachHealthcheckTimestamp(workerID = %v): %v\n", worker.ID, err))
				}
			}

			time.Sleep(every)
		}
	}()
}
