package usecases

import (
	"fmt"
	"time"

	"bitbucket.org/saygames/highkick/internal/domain"
	"bitbucket.org/saygames/highkick/internal/job_logs"
	"bitbucket.org/saygames/highkick/internal/jobs"
)

// Log message for a job
func Log(job *domain.Job, message string) {
	maxContentLength := len(message)
	if maxContentLength > 65535 {
		maxContentLength = 65535
	}

	// TODO: Idea! Smart-insert (like bulk insert with single connection background worker)
	if err := job_logs.Repo.Save(&job_logs.JobLog{
		JobID:     job.ID,
		JobPath:   job.Path,
		Content:   message[:maxContentLength], // Content is MySQL TEXT
		CreatedAt: time.Now(),
	}); err != nil {
		fmt.Println("[Highkick] [Log(job)] [1] ", err)
	}

	// DONE: Optimization. Who really cares the exact number of log records? Lets skip this hard-updates and reduce CO2
	if job.LogsCount == 0 {
		job.LogsCount = 1
		if err := jobs.Repo.DB.UpdateColumns(job, "logs_count"); err != nil {
			fmt.Println("[Highkick] [Log(job)] [2] ", err)
		}
	}
}

// SetOutput preserves string value by key in job's dictionary
func SetOutput(job *domain.Job, key string, value string) {
	output := job.PRIVATE_GetOutput()
	output[key] = value
	job.PRIVATE_SetOutput(output)
	if err := jobs.Repo.Save(job); err != nil {
		fmt.Println("[Highkick] [SetOutput(job)] ", err)
	}
}

func SetOutputRaw(job *domain.Job, output interface{}) {
	job.SetOutputRaw(output)
	if err := jobs.Repo.Save(job); err != nil {
		fmt.Println("[Highkick] [SetOutputRaw(job)] ", err)
	}
}

func GetOutputRaw(jobID int, to interface{}) error {
	job, err := jobs.Repo.SelectOne(jobID)
	if err != nil {
		return err
	}
	if err := job.CastOutput(to); err != nil {
		return err
	}
	return nil
}

// GetOutput gets string by key from job's dictionary
func GetOutputByKey(jobID int, key string) *string {
	output := GetOutput(jobID)
	if output == nil {
		return nil
	}
	value, exists := (*output)[key]
	if !exists {
		return nil
	}
	valueStr := value.(string)
	return &valueStr
}

// GetOutput gets string by key from job's dictionary
func GetOutput(jobID int) *domain.JSONDictionary {
	job, _ := jobs.Repo.SelectOne(jobID)
	if job != nil {
		output := job.PRIVATE_GetOutput()
		return &output
	}
	return nil
}
