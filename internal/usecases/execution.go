package usecases

import (
	"fmt"
	"runtime/debug"
	"sync"
	"time"

	"bitbucket.org/saygames/highkick/internal/domain"
	"bitbucket.org/saygames/highkick/internal/job_logs"
	"bitbucket.org/saygames/highkick/internal/jobs"
	"bitbucket.org/saygames/highkick/internal/workers"
)

type WorkerParams struct {
	JobsToHandle    domain.JobsToHandle
	DebugLog        func(string)
	PollingInterval time.Duration
}

func RunWorker(worker workers.Worker, params WorkerParams) {
	fmt.Printf("[HIGHKICK] Worker %v %v %v started\n", worker.ID, worker.SID, worker.ProcessSID)

	go func() {
		// Loop forever (until expicit break)
		every := 30 * time.Second
		if params.PollingInterval.Seconds() > 0 {
			every = params.PollingInterval
		}

		for {
			// Execute logic and return if the loop should be breaked or not
			shouldBreak := func() bool {
				defer func() {
					if r := recover(); r != nil {
						params.DebugLog(fmt.Sprintf("[RunWorker/RECOVER] %v", r))
					}
				}()

				worker, err := workers.Repo.SelectOne(worker.ID)
				if err != nil {
					params.DebugLog(fmt.Sprintf("[RunWorker/Err1] %v", err))
					return false
				}
				if worker == nil {
					params.DebugLog(fmt.Sprintf("[RunWorker] No worker %v found\n", worker.ID))
					return true // means shouldBreak!
				}

				scheduledJobs, err := jobs.Repo.Select(jobs.QueryBuilder{
					Status: &domain.JobStatuses.Scheduled,
					ForWorker: &jobs.QueryBuilder_ForWorker{
						JobsToHandle: params.JobsToHandle,
						WorkerSID:    worker.SID,
					},
				})
				if err != nil {
					params.DebugLog(fmt.Sprintf("[RunWorker/Err2] %v", err))
					return false
				}

				wg := sync.WaitGroup{}
				for _, scheduledJob := range scheduledJobs {
					job := scheduledJob // need to copy

					if worker.Stopped && job.IsRoot() {
						// stopped worker should not run new root-jobs
						continue
					} else if job.Sid == HIGHKICK_STOP_WORKER {
						if shallRunJobOnThisWorker, err := ShallRunJobOnThisWorker(job, worker.ID); err == nil {
							if *shallRunJobOnThisWorker == false {
								// do nothing
								continue
							}
							// otherwise - execute
						} else {
							params.DebugLog(fmt.Sprintf("[RunWorker/Err3] %v", err))
							return false
						}
					}

					wg.Add(1)
					onJobUpdatedCallback := func() {
						wg.Done()
					}
					go func() {
						job.WorkerID = worker.ID
						// RunSync(job)
						_, err := RunSyncWithJobUpdateCallback(job, &onJobUpdatedCallback)
						if err != nil {
							fmt.Printf("[HIGHKICK] Job failed %v: %+v\n", job.Sid, err)
						}
					}()
				}
				wg.Wait()

				return false
			}()
			if shouldBreak {
				break
			}

			time.Sleep(every)
		}
		fmt.Printf("[HIGHKICK] Worker %v %v %v stopped\n", worker.ID, worker.SID, worker.ProcessSID)
	}()
}

func retry(jobID int, run func(domain.Job) (*domain.Job, error)) (*domain.Job, error) {
	job, err := jobs.Repo.SelectOne(jobID)
	if err != nil {
		return nil, err
	}
	return run(*job)
}
func RetrySync(jobID int) (*domain.Job, error) {
	return retry(jobID, RunSync)
}
func RetryAsync(jobID int) (*domain.Job, error) {
	return retry(jobID, RunAsync)
}

func attemptRetry(jobID int, run func(domain.Job) (*domain.Job, error)) (*domain.Job, error) {
	job, err := jobs.Repo.SelectOne(jobID)
	if err != nil {
		return nil, err
	}
	if job.RetriesLeft == 0 {
		return job, fmt.Errorf("job %v has no retries left", jobID)
	}
	job.RetriesLeft = job.RetriesLeft - 1
	if err := jobs.Repo.Save2(job); err != nil {
		return job, err
	}
	return run(*job)
}
func AttemptRetrySync(jobID int) (*domain.Job, error) {
	return attemptRetry(jobID, RunSync)
}
func AttemptRetryAsync(jobID int) (*domain.Job, error) {
	return attemptRetry(jobID, RunAsync)
}

func RunAsync(job domain.Job) (*domain.Job, error) {
	job.Status = domain.JobStatuses.Scheduled
	job.CreatedAt = time.Now()
	if err := jobs.Repo.Save2(&job); err != nil {
		return nil, err
	}
	return &job, nil
}

func RunSyncWithJobUpdateCallback(job domain.Job, onJobUpdatedCallback *func()) (*domain.Job, error) {
	workers.IncrementRunningJobsCount(job.WorkerID, 1)
	defer workers.IncrementRunningJobsCount(job.WorkerID, -1)

	job.Status = domain.JobStatuses.Initial
	job.CreatedAt = time.Now()
	if err := jobs.Repo.Save2(&job); err != nil {
		if onJobUpdatedCallback != nil {
			(*onJobUpdatedCallback)()
		}
		return nil, err
	}
	if onJobUpdatedCallback != nil {
		(*onJobUpdatedCallback)()
	}

	_, err := runJobByWorkers(&job)
	return &job, err
}

func RunSync(job domain.Job) (*domain.Job, error) {
	return RunSyncWithJobUpdateCallback(job, nil)
}

func RunAsWrappedFunction(job *domain.Job, perform func(*domain.Job) error) error {
	job.Status = domain.JobStatuses.Initial
	job.CreatedAt = time.Now()
	if err := jobs.Repo.Save2(job); err != nil {
		return err
	}

	_, err := runJob(job, perform)
	return err
}

func runJobByWorkers(job *domain.Job) (*domain.Job, error) {
	var jobMeta *domain.JobMeta
	if w, exists := jobMetas[job.Sid]; exists == true {
		jobMeta = &w
	} else {
		return nil, fmt.Errorf("No worker found for %v", job.Sid)
	}

	return runJob(job, jobMeta.Perform)
}

func runJob(job *domain.Job, perform func(*domain.Job) error) (*domain.Job, error) {

	job.Status = domain.JobStatuses.Processing
	now := time.Now()
	job.StartedAt = &now
	if err := jobs.Repo.Save2(job); err != nil {
		return nil, err
	}

	execute := func() (resultErr error) {
		defer func() {
			if rec := recover(); rec != nil {
				err := fmt.Errorf("%+v", rec)
				failJob(*job, err)
				resultErr = err
			}
		}()

		BroadcastJobUpdate(*job, nil)
		clearJob(*job)

		err := perform(job)

		if err == nil {
			completeJob(*job)
		} else {
			failJob(*job, err)
		}

		return err
	}

	fmt.Printf("[HIGHKICK] Running job %v\n", job.Sid)
	err := execute()
	return job, err
}

// completeJob is called on job's completion
func completeJob(job domain.Job) {
	job.Status = domain.JobStatuses.Completed
	now := time.Now()
	job.FinishedAt = &now
	if err := jobs.Repo.Save2(&job); err != nil {
		fmt.Println("[HIGHKICK_ERROR] [completeJob] [1] ", err)
	}
	BroadcastJobUpdate(job, nil)
}

// completeJob is called when job has failed
func failJob(job domain.Job, err error) {
	job.Status = domain.JobStatuses.Failed
	now := time.Now()
	job.FinishedAt = &now
	if err := jobs.Repo.Save2(&job); err != nil {
		fmt.Println("[HIGHKICK_ERROR] [failJob] [1] ", err)
	}

	fmt.Printf("[JOB] [%v] %v\n", job.Sid, err.Error())
	fmt.Printf("[JOB] [%v] Stacktrace: %v\n", job.Sid, string(debug.Stack()))
	Log(&job, fmt.Sprintf("[ERROR] %v. Stack: %v", err.Error(), string(debug.Stack())))
	BroadcastJobUpdate(job, err)
}

func clearJob(job domain.Job) {
	items, err := jobs.Repo.Select(jobs.QueryBuilder{
		SubtreeOf: &job,
	})
	if err != nil {
		fmt.Println("[HIGHKICK_ERROR] [clearJob] [1] ", err)
		return
	}

	for _, j := range items {
		if err := job_logs.Repo.DestroyAll(job_logs.QueryBuilder{
			JobID: &j.ID,
		}); err != nil {
			fmt.Println("[HIGHKICK_ERROR] [clearJob] [2] ", err)
			continue
		}
		if j.ID == job.ID {
			continue
		}
		if err := jobs.Repo.Destroy(&j); err != nil {
			fmt.Println("[HIGHKICK_ERROR] [clearJob] [3] ", err)
			continue
		}
	}
}
