package usecases

import (
	"time"

	"bitbucket.org/saygames/highkick/internal/domain"
	"bitbucket.org/saygames/highkick/internal/jobs"
)

type GetJobIDsParams struct {
	IDs      []int
	SID      string
	Statuses []domain.JobStatus
	Since    *time.Time
	Till     *time.Time
}

func GetJobIDs(params GetJobIDsParams) ([]int, error) {
	jobs, err := jobs.Repo.Select(jobs.QueryBuilder{
		IDs:      &params.IDs,
		SID:      &params.SID,
		Statuses: &params.Statuses,
		Since:    params.Since,
		Till:     params.Till,
	})
	if err != nil {
		return nil, err
	}
	jobIDs := []int{}
	for _, job := range jobs {
		jobIDs = append(jobIDs, job.ID)
	}
	return jobIDs, nil
}

func GetParent(job domain.Job) (*domain.Job, error) {
	if parentID := job.GetParentID(); parentID > 0 {
		return jobs.Repo.SelectOne(parentID)
	} else {
		return nil, nil
	}
}
