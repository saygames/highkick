package domain

type PubSubMessage struct {
	Job   Job
	Error error
}
