package domain

type JobStatus string

var JobStatuses = struct {
	Scheduled  JobStatus
	Initial    JobStatus
	Processing JobStatus
	Failed     JobStatus
	Completed  JobStatus
}{
	Scheduled:  "scheduled",
	Initial:    "initial",
	Processing: "processing",
	Failed:     "failed",
	Completed:  "completed",
}
