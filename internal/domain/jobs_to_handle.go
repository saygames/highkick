package domain

type JobsToHandle struct {
	Only   []string
	Except []string
}
