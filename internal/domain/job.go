package domain

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"time"
)

//go:generate reform
//reform:jobs
type Job struct {
	ID  int    `reform:"id,pk" json:"id"`
	Sid string `reform:"sid" json:"sid"`

	Path                 string `reform:"path" json:"path"`
	FullPathJSONAsString string `reform:"full_path" json:"-"`
	FullPath             []int

	Input         *string    `reform:"input" json:"-"`
	Output        *string    `reform:"output" json:"output"`
	Status        JobStatus  `reform:"status" json:"status"`
	TreeStatus    *JobStatus `json:"treeStatus"`
	RetriesLeft   int        `reform:"retries_left" json:"retriesLeft"`
	LogsCount     int        `reform:"logs_count" json:"logsCount"`
	RunWithWorker string     `reform:"run_with_worker"`
	WorkerID      int        `reform:"worker_id"`
	StartedAt     *time.Time `reform:"started_at"`
	FinishedAt    *time.Time `reform:"finished_at"`
	CreatedAt     time.Time  `reform:"created_at" json:"createdAt"`
}

func (job Job) FullPathWithoutZeros() []int {
	result := []int{}
	for _, v := range job.FullPath {
		if v == 0 {
			continue
		}
		result = append(result, v)
	}
	return result
}

func (job Job) FullPathWithZeros(n int) []int {
	result := job.FullPath
	for i := 0; i < n-len(job.FullPath); i++ {
		result = append(result, 0)
	}
	return result
}

// GetInput is getter for Input
func (job *Job) GetInput() JSONDictionary {
	var dict JSONDictionary
	_ = json.Unmarshal([]byte(*job.Input), &dict)
	return dict
}

// SetInput is setter for Input
func (job *Job) PRIVATE_SetInput(dict JSONDictionary) string {
	valueAsBytes, _ := json.Marshal(dict)
	value := string(valueAsBytes)
	job.Input = &value
	return value
}

// GetOutput is getter for Output
func (job *Job) PRIVATE_GetOutput() JSONDictionary {
	dict := JSONDictionary{}
	if job.Output == nil {
		return dict
	}
	_ = json.Unmarshal([]byte(*job.Output), &dict)
	return dict
}

// SetOutput is setter for Output
func (job *Job) PRIVATE_SetOutput(dict JSONDictionary) string {
	valueAsBytes, _ := json.Marshal(dict)
	value := string(valueAsBytes)
	job.Output = &value
	return value
}

func (job *Job) SetOutputRaw(output interface{}) string {
	valueAsBytes, _ := json.Marshal(output)
	value := string(valueAsBytes)
	job.Output = &value
	return value
}

func (job *Job) ParseInput(to interface{}) error {
	if err := json.Unmarshal([]byte(*job.Input), to); err != nil {
		return err
	}
	return nil
}

func (job *Job) CastOutput(to interface{}) error {
	if job.Output == nil {
		// result = nil
		return nil
	}
	if err := json.Unmarshal([]byte(*job.Output), to); err != nil {
		return err
	}
	return nil
}

// GetRootID returns root ID for this job's tree
func (job *Job) GetRootID() int {
	if job.Path == "" {
		return job.ID
	}
	ids := strings.Split(job.Path, "/")
	rootID, _ := strconv.Atoi(ids[0])
	return rootID
}

func (job *Job) GetParentID() int {
	p := job.FullPathWithoutZeros()
	if len(p) >= 1 {
		return p[len(p)-1]
	}
	return 0
}

// SetParent initialize job's path by it's parent
func (job *Job) PRIVATE_SetParent(parent *Job) {
	if parent == nil {
		return
	}

	// Path
	{
		parentIDString := fmt.Sprintf("%v", parent.ID)
		if parent.Path == "" {
			job.Path = parentIDString
		} else {
			job.Path = strings.Join([]string{parent.Path, parentIDString}, "/")
		}
	}

	// Full path
	{
		job.FullPath = append(parent.FullPathWithoutZeros(), parent.ID)
		fmt.Println("job.FullPath", job.FullPath)
	}

	// Run with worker
	// We should propogate this field to every child so all job stack to run with this worker
	job.RunWithWorker = parent.RunWithWorker
}

// IsRoot returns whether the job is root or not
func (job *Job) IsRoot() bool {
	rootJobID := job.GetRootID()
	return rootJobID == job.ID
}

// IsCompleted returns if job is completed
func (job *Job) IsCompleted() bool {
	return job.Status == JobStatuses.Completed || job.Status == JobStatuses.Failed
}

// IsFailed returns if job is failed
func (job *Job) IsFailed() bool {
	return job.Status == JobStatuses.Failed
}

// NewJob is a builder helper
func NewJob(sid string, input JSONDictionary, parent *Job) Job {
	job := Job{
		Sid: sid,
	}
	job.PRIVATE_SetInput(input)
	job.PRIVATE_SetParent(parent)
	return job
}

// NewJob2 is a builder helper
func NewJob2(sid string, input interface{}, parent *Job) Job {
	job := Job{
		Sid: sid,
	}
	inputAsJSONDictionary, _ := StructToJSONDictionary(input)
	job.PRIVATE_SetInput(*inputAsJSONDictionary)
	job.PRIVATE_SetParent(parent)
	return job
}

func (job *Job) IsParentOf(childJob Job) bool {
	pathWithParentID := strings.Trim(fmt.Sprintf("%v/%v", job.Path, job.ID), "/")
	return strings.Contains(childJob.Path, pathWithParentID)
}

func (job *Job) IsChildOf(parentJob Job) bool {
	pathWithParentID := strings.Trim(fmt.Sprintf("%v/%v", parentJob.Path, parentJob.ID), "/")
	return strings.Contains(job.Path, pathWithParentID)
}

func (job *Job) Identificator() string {
	if job.Input != nil {
		return fmt.Sprintf("%s-%+v", job.Sid, *job.Input)
	}
	return fmt.Sprintf("%s", job.Sid)
}

func (job Job) WithWorker(workerName string) Job {
	job.RunWithWorker = workerName
	return job
}

func (job Job) WithMaxRetry(maxRetry int) Job {
	job.RetriesLeft = maxRetry
	return job
}
