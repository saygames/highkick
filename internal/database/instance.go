package database

import (
	"database/sql"

	"bitbucket.org/saygames/highkick/internal/job_logs"
	"bitbucket.org/saygames/highkick/internal/job_stats"
	"bitbucket.org/saygames/highkick/internal/jobs"
	"bitbucket.org/saygames/highkick/internal/schedulers"
	"bitbucket.org/saygames/highkick/internal/workers"
)

// Keeps singleton instance of Manager
var (
	Manager manager
)

type DatabaseEngine string

var DatabaseEngines = struct {
	MySQL   DatabaseEngine
	MariaDB DatabaseEngine
}{
	MySQL:   "mysql",
	MariaDB: "mariadb",
}

type DatabaseOptions struct {
	DB            *sql.DB
	Engine        DatabaseEngine
	Database      string
	RunMigrations bool
	EnableLogging bool
}

// Setup inits singleton
func Setup(options DatabaseOptions) {
	Manager.Setup(options)

	jobs.InitializeRepo(Manager.DBR)
	job_logs.InitializeRepo(Manager.DBR)
	schedulers.InitializeRepo(Manager.DBR)
	workers.InitializeRepo(Manager.DBR)
	job_stats.InitializeRepo(Manager.DBR)
}

// Close closes all connections
func Close() {
	if db := Manager.DB; db != nil {
		db.Close()
	}
}
