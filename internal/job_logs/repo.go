package job_logs

import (
	"fmt"

	"gopkg.in/reform.v1"

	repositoryLib "bitbucket.org/saygames/repository/pkg/mysql"
)

var Repo repositoryLib.Repository[JobLog, *JobLog]

func InitializeRepo(dbr *reform.DB) {
	Repo = repositoryLib.Repository[JobLog, *JobLog]{
		DB:        dbr,
		View:      JobLogTable,
		EnableLog: false,
	}
	fmt.Println("[Repo] [JobLogs] Initialized")
}
