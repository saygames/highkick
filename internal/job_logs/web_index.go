package job_logs

import (
	"encoding/json"
	"net/http"

	"github.com/gin-gonic/gin"
)

// Index is Index
func Index(c *gin.Context) {
	// Parse input
	qb := QueryBuilder{}
	if err := json.Unmarshal([]byte(c.Query("filters")), &qb); err != nil {
		panic(err)
	}

	items, err := Repo.Select(qb)
	if err != nil {
		panic(err)
	}

	c.JSON(http.StatusOK, struct {
		Items []JobLog
	}{
		Items: items,
	})
}
