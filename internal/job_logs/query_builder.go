package job_logs

import (
	"fmt"
	"strings"
	"time"
)

type QueryBuilder struct {
	ID            *int
	JobID         *int
	JobIDLessThan *int
	IDSince       *int
	Since         *time.Time
	Till          *time.Time
	Limit         *int
	Page          *int
	PerPage       *int
}

func (f QueryBuilder) Select() *[]string {
	return nil
}

func (f QueryBuilder) ForceIndex() *string {
	return nil
}

func (f QueryBuilder) Join() *string {
	return nil
}

func (f QueryBuilder) Where() string {
	clauses := []string{"true"}

	if f.ID != nil {
		clauses = append(clauses, fmt.Sprintf("(id = %v)", *f.ID))
	}
	if f.IDSince != nil {
		clauses = append(clauses, fmt.Sprintf("(id >= %v)", *f.IDSince))
	}
	if f.Since != nil {
		clauses = append(clauses, fmt.Sprintf("(created_at >= '%v')", f.Since.Format("2006-01-02 15:04:05")))
	}
	if f.Till != nil {
		clauses = append(clauses, fmt.Sprintf("(created_at <= '%v')", f.Till.Format("2006-01-02 15:04:05")))
	}

	if f.JobID != nil {
		clauses = append(clauses, fmt.Sprintf("(job_id = %v)", *f.JobID))
	}
	if f.JobIDLessThan != nil {
		clauses = append(clauses, fmt.Sprintf("(job_id < %v)", *f.JobIDLessThan))
	}

	return strings.Join(clauses, " AND ")
}

func (qb QueryBuilder) GroupBy() *[]string {
	return nil
}

func (qb QueryBuilder) OrderBy() *string {
	orderBy := "id ASC"
	return &orderBy
}

func (qb QueryBuilder) Pagination() *struct {
	Page    int
	PerPage int
} {
	if qb.Page != nil && qb.PerPage != nil {
		return &struct {
			Page    int
			PerPage int
		}{
			Page:    *qb.Page,
			PerPage: *qb.PerPage,
		}
	}
	return nil
}
