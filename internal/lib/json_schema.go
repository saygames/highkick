package lib

import (
	"github.com/invopop/jsonschema"
)

func GetJSONSchemaFor(v interface{}) (*string, error) {
	schema := jsonschema.Reflect(v)

	{
		t, err := schema.MarshalJSON()
		if err != nil {
			return nil, err
		}
		result := string(t)
		return &result, nil
	}

	// {
	// 	var key string = ""
	// 	for k, _ := range schema.Definitions {
	// 		key = k
	// 		break
	// 	}
	// 	t, err := schema.Definitions[key].MarshalJSON()
	// 	if err != nil {
	// 		return nil, err
	// 	}
	// 	result := string(t)
	// 	return &result, nil
	// }

}
