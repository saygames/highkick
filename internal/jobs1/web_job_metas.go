package jobs1

import (
	"net/http"

	"bitbucket.org/saygames/highkick/internal/usecases"

	"github.com/gin-gonic/gin"
)

func IndexJobMetas(c *gin.Context) {
	jobs := usecases.GetJobs()

	contracts := []map[string]string{}
	for _, job := range jobs {
		contract := job.ToContract()
		contracts = append(contracts, contract)
	}
	// siblingsStatus := repo.GetSiblingsDetailedStatus(job)
	c.JSON(http.StatusOK, map[string]interface{}{
		"items": contracts,
	})
}
