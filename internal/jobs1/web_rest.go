package jobs1

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"

	"bitbucket.org/saygames/highkick/internal/domain"
	"bitbucket.org/saygames/highkick/internal/job_logs"
	"bitbucket.org/saygames/highkick/internal/jobs"
	"bitbucket.org/saygames/highkick/internal/usecases"
)

// Destroy .
func Destroy(c *gin.Context) {
	params := struct {
		JobID int `uri:"job_id" binding:"required"`
	}{}
	if err := c.ShouldBindUri(&params); err != nil {
		panic(err)
	}

	job, err := jobs.Repo.SelectOne(params.JobID)
	if err != nil {
		panic(err)
	}

	items, err := jobs.Repo.Select(jobs.QueryBuilder{
		SubtreeOf: job,
	})
	if err != nil {
		panic(err)
	}

	for _, j := range items {
		if err := job_logs.Repo.DestroyAll(job_logs.QueryBuilder{
			JobID: &j.ID,
		}); err != nil {
			panic(err)
		}
		if err := jobs.Repo.Destroy(&j); err != nil {
			panic(err)
		}
	}

	c.JSON(http.StatusOK, struct{}{})
}

// Show .
func Show(c *gin.Context) {
	params := struct {
		JobID int `uri:"job_id" binding:"required"`
	}{}
	if err := c.ShouldBindUri(&params); err != nil {
		panic(err)
	}

	job, err := jobs.Repo.SelectOne(params.JobID)
	if err != nil {
		panic(err)
	}

	siblingsStatus, err := usecases.GetSiblingsDetailedStatus(job)
	if err != nil {
		panic(err)
	}

	c.JSON(http.StatusOK, map[string]interface{}{
		"job":      job,
		"siblings": *siblingsStatus,
	})
}

func UpdateHandler(c *gin.Context) {
	params := struct {
		JobID int `uri:"job_id" binding:"required"`
	}{}
	if err := c.ShouldBindUri(&params); err != nil {
		panic(err)
	}

	input := struct {
		Input         domain.JSONDictionary `json:"input" binding:"required"`
		RunWithWorker string                `binding:"omitempty"`
	}{}
	err := c.ShouldBindBodyWith(&input, binding.JSON)
	if err != nil {
		panic(err)
	}

	job, err := jobs.Repo.SelectOne(params.JobID)
	if err != nil {
		panic(err)
	}

	job.PRIVATE_SetInput(input.Input)
	job.RunWithWorker = input.RunWithWorker

	if err := jobs.Repo.Save(job); err != nil {
		panic(err)
	}

	c.JSON(http.StatusOK, nil)
}
