package jobs1

import (
	"net/http"

	"bitbucket.org/saygames/highkick/internal/domain"
	"bitbucket.org/saygames/highkick/internal/jobs"
	"bitbucket.org/saygames/highkick/internal/usecases"

	"github.com/gin-gonic/gin"
)

// RetryFailedLeaves .
func RetryFailedLeaves(c *gin.Context) {
	params := struct {
		JobID int `uri:"job_id" binding:"required"`
	}{}
	if err := c.ShouldBindUri(&params); err != nil {
		panic(err)
	}

	job, err := jobs.Repo.SelectOne(params.JobID)
	if err != nil {
		panic(err)
	}

	treeJobs, err := jobs.Repo.Select(jobs.QueryBuilder{
		SubtreeOf: job,
	})
	if err != nil {
		panic(err)
	}

	for _, treeJob := range treeJobs {
		if job.ID == treeJob.ID {
			continue
		}

		isLeave := true
		for _, treeJob2 := range treeJobs {
			if treeJob.IsParentOf(treeJob2) {
				isLeave = false
				break
			}
		}

		if !isLeave {
			continue
		}

		if treeJob.Status == domain.JobStatuses.Failed {
			go usecases.RunSync(treeJob)
		}
	}

	c.JSON(http.StatusOK, struct{}{})
}
