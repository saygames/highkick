package jobs1

import (
	"net/http"

	"bitbucket.org/saygames/highkick/internal/domain"
	"bitbucket.org/saygames/highkick/internal/jobs"
	"bitbucket.org/saygames/highkick/internal/usecases"

	"encoding/json"

	"github.com/gin-gonic/gin"
)

// Index is Index
func ActiveRootsHandler(ctx *gin.Context) {
	// Parse input
	qb := jobs.QueryBuilder{}
	if err := json.Unmarshal([]byte(ctx.Query("filters")), &qb); err != nil {
		panic(err)
	}

	// Get all active job nodes
	activeJobs, err := jobs.Repo.Select(jobs.QueryBuilder{
		Statuses: &[]domain.JobStatus{domain.JobStatuses.Processing, domain.JobStatuses.Scheduled, domain.JobStatuses.Initial},
	})
	if err != nil {
		panic(err)
	}

	// Extend active root ids to query builder
	childStatusForRootID := map[int]domain.JobStatus{}
	rootJobIDsMap := map[int]bool{}
	roots := []domain.Job{}
	{
		if len(activeJobs) > 0 {
			for _, activeJob := range activeJobs {
				rootID := activeJob.GetRootID()
				rootJobIDsMap[rootID] = true
				childStatusForRootID[rootID] = activeJob.Status
			}
			rootJobIDs := []int{}
			for rootJobID := range rootJobIDsMap {
				rootJobIDs = append(rootJobIDs, rootJobID)
			}
			qb.IDs = &rootJobIDs

			truly := true
			qb.IsRoot = &truly

			t, err := jobs.Repo.Select(qb)
			if err != nil {
				panic(err)
			}
			roots = t
		}
	}

	items := []domain.Job{}
	for _, root := range roots {
		// Strategy A
		// Resolve from child statuses
		childStatus := childStatusForRootID[root.ID]
		root.TreeStatus = &childStatus

		// Strategy B
		// Resolve for each root node separatly
		// treeStatus, err := usecases.GetJobTreeStatus(root)
		// if err != nil {
		// 	panic(err)
		// }
		// root.TreeStatus = treeStatus

		items = append(items, root)
	}

	// Get meta info
	childrenStats := []usecases.ChildrenStat{}
	for _, item := range items {
		childrenStat, err := usecases.GetChildrenStat(&item)
		if err != nil {
			panic(err)
		}
		childrenStats = append(childrenStats, *childrenStat)
	}

	ctx.JSON(http.StatusOK, struct {
		Items         []domain.Job
		ChildrenStats []usecases.ChildrenStat
	}{
		Items:         items,
		ChildrenStats: childrenStats,
	})
}
