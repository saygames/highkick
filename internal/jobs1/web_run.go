package jobs1

import (
	"net/http"

	"bitbucket.org/saygames/highkick/internal/domain"
	"bitbucket.org/saygames/highkick/internal/usecases"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
)

func Run(c *gin.Context) {
	input := struct {
		SID           string                `binding:"required"`
		Input         domain.JSONDictionary `binding:"required"`
		RunWithWorker string                `binding:"omitempty"`
	}{}
	err := c.ShouldBindBodyWith(&input, binding.JSON)
	if err != nil {
		panic(err)
	}

	newJob := domain.NewJob(input.SID, input.Input, nil)
	newJob.RunWithWorker = input.RunWithWorker

	job, err := usecases.RunAsync(newJob)
	if err != nil {
		panic(err)
	}

	c.JSON(http.StatusOK, map[string]interface{}{
		"job": *job,
	})
}
