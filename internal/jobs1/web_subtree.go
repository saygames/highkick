package jobs1

import (
	"net/http"

	"bitbucket.org/saygames/highkick/internal/jobs"
	"github.com/gin-gonic/gin"
)

// Subtree is Subtree
func Subtree(c *gin.Context) {
	params := struct {
		JobID int `uri:"job_id" binding:"required"`
	}{}
	if err := c.ShouldBindUri(&params); err != nil {
		panic(err)
	}

	job, err := jobs.Repo.SelectOne(params.JobID)
	if err != nil {
		panic(err)
	}

	jobs, err := jobs.Repo.Select(jobs.QueryBuilder{
		SubtreeOf: job,
	})
	if err != nil {
		panic(err)
	}

	c.JSON(http.StatusOK, jobs)
}
