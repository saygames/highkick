package jobs1

import (
	"net/http"

	"bitbucket.org/saygames/highkick/internal/jobs"
	"github.com/AlekSi/pointer"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
)

func GetLastHandler(c *gin.Context) {
	input := struct {
		SID string `binding:"required"`
	}{}
	err := c.ShouldBindBodyWith(&input, binding.JSON)
	if err != nil {
		panic(err)
	}

	items, err := jobs.Repo.Select(jobs.QueryBuilder{
		SID:           &input.SID,
		OrderByIDDesc: pointer.ToBool(true),
		Page:          pointer.ToInt(1),
		PerPage:       pointer.ToInt(1),
	})
	if err != nil {
		panic(err)
	}

	if len(items) == 1 {
		c.JSON(http.StatusOK, map[string]interface{}{
			"job": items[0],
		})
	} else {
		c.JSON(http.StatusOK, map[string]interface{}{
			"job": nil,
		})
	}
}
