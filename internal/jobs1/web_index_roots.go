package jobs1

import (
	"net/http"
	"strconv"

	"bitbucket.org/saygames/highkick/internal/jobs"
	"bitbucket.org/saygames/highkick/internal/usecases"

	"encoding/json"

	"github.com/gin-gonic/gin"
)

// Index is Index
func IndexRootsHandler(ctx *gin.Context) {
	page, _ := strconv.Atoi(ctx.Query("page"))
	limit := 50
	truly := true

	qb := jobs.QueryBuilder{}
	if err := json.Unmarshal([]byte(ctx.Query("filters")), &qb); err != nil {
		panic(err)
	}
	qb.Page = &page
	qb.PerPage = &limit
	qb.OrderByIDDesc = &truly
	qb.IsRoot = &truly

	roots, err := jobs.Repo.Select(qb)
	if err != nil {
		panic(err)
	}

	for _, root := range roots {
		treeStatus, err := usecases.GetJobTreeStatus(root)
		if err != nil {
			panic(err)
		}
		root.TreeStatus = treeStatus
	}

	ctx.JSON(http.StatusOK, roots)
}
