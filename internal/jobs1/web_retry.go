package jobs1

import (
	"net/http"

	"bitbucket.org/saygames/highkick/internal/usecases"

	"github.com/gin-gonic/gin"
)

// Retry is Retry
func Retry(c *gin.Context) {
	params := struct {
		JobID int `uri:"job_id" binding:"required"`
	}{}
	if err := c.ShouldBindUri(&params); err != nil {
		panic(err)
	}

	if _, err := usecases.RetryAsync(params.JobID); err != nil {
		panic(err)
	}
	c.JSON(http.StatusOK, struct{}{})
}
