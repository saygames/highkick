package jobs1

import (
	"fmt"

	"bitbucket.org/saygames/highkick/internal/domain"
	"bitbucket.org/saygames/highkick/internal/job_logs"
	"bitbucket.org/saygames/highkick/internal/jobs"
	"bitbucket.org/saygames/highkick/internal/usecases"
	"github.com/tidwall/gjson"
)

const HIGHKICK_CLEAN_UP = "HIGHKICK/CLEAN_UP"

func init() {
	inputJSONSchema := `{
		"type": "object",
		"properties": {
			"NewestJobsCountToKeep": { "type": "number" },
			"SID": { "type": "string" }
		},
		"required": ["NewestJobsCountToKeep"]
	}`
	usecases.Register(domain.JobMeta{
		SID:             HIGHKICK_CLEAN_UP,
		Title:           HIGHKICK_CLEAN_UP,
		Perform:         CleanUpWorker,
		InputJSONSchema: &inputJSONSchema,
	})
}

func CleanUpWorker(job *domain.Job) error {
	for _, key := range []string{"NewestJobsCountToKeep"} {
		if !gjson.Get(*job.Input, key).Exists() {
			return fmt.Errorf("%v is required", key)
		}
	}
	newestJobsCountToKeep := int(gjson.Get(*job.Input, "NewestJobsCountToKeep").Int())
	inputSID := gjson.Get(*job.Input, "SID").String()

	// same as offset!
	page := newestJobsCountToKeep
	perPage := 1
	truly := true

	var qbSID *string = nil
	if inputSID != "" {
		qbSID = &inputSID
	}

	var thresholdJob *domain.Job
	{
		t, err := jobs.Repo.Select(jobs.QueryBuilder{
			Page:          &page,
			PerPage:       &perPage,
			OrderByIDDesc: &truly,
			SID:           qbSID,
		})
		if err != nil {
			return err
		}
		if len(t) > 0 {
			_thresholdJob := t[0]
			thresholdJob = &_thresholdJob
		} else {
			usecases.Log(job, fmt.Sprintf("Jobs count is less than %v", newestJobsCountToKeep))
			return nil
		}
	}
	usecases.Log(job, fmt.Sprintf("Threshold job id = %v", thresholdJob.ID))

	// Issue: we remove all logs w/o regregting `inputSID`
	//
	if err := job_logs.Repo.DestroyAll(job_logs.QueryBuilder{
		JobIDLessThan: &thresholdJob.ID,
	}); err != nil {
		return err
	}
	usecases.Log(job, fmt.Sprintf("Job logs: cleaned up"))

	if err := jobs.Repo.DestroyAll(jobs.QueryBuilder{
		IDLessThan: &thresholdJob.ID,
		SID:        qbSID,
	}); err != nil {
		return err
	}
	usecases.Log(job, fmt.Sprintf("Jobs: cleaned up"))

	return nil
}
