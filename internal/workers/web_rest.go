package workers

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
)

func Index(ctx *gin.Context) {
	items, err := Repo.Select(QueryBuilder{})
	if err != nil {
		panic(err)
	}

	ctx.JSON(http.StatusOK, items)
}

func Show(ctx *gin.Context) {
	idStr := ctx.Param("id")
	idInt64, err := strconv.ParseInt(idStr, 10, 64)
	if err != nil {
		panic(err)
	}

	item, err := Repo.SelectOne(int(idInt64))
	if err != nil {
		panic(err)
	}

	ctx.JSON(http.StatusOK, *item)
}

func CreateUpdate(c *gin.Context) {
	var item Worker
	err := c.ShouldBindBodyWith(&item, binding.JSON)
	if err != nil {
		panic(err)
	}

	if err := Repo.Save(&item); err != nil {
		panic(err)
	}

	c.JSON(http.StatusOK, item)
}

// Destroy .
func Destroy(c *gin.Context) {
	idStr := c.Param("id")
	idInt64, err := strconv.ParseInt(idStr, 10, 64)
	if err != nil {
		panic(err)
	}

	item, err := Repo.SelectOne(int(idInt64))
	if err != nil {
		panic(err)
	}

	if err := Repo.Destroy(item); err != nil {
		panic(err)
	}

	c.JSON(http.StatusOK, struct{}{})
}
