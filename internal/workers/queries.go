package workers

import (
	"fmt"
	"sync"
	"time"
)

var incrementMU = sync.Mutex{}

func IncrementRunningJobsCount(workerID int, incr int) error {
	// GIL
	// Todo: dedicated mutex for each worker
	incrementMU.Lock()
	defer incrementMU.Unlock()

	q := fmt.Sprintf("UPDATE %v SET running_jobs_count = running_jobs_count + (%v) WHERE id = %v", WorkerTable.Name(), incr, workerID)
	// fmt.Println(">>>", q)
	if _, err := Repo.DB.Exec(q); err != nil {
		return err
	}
	return nil
}

func TrachHealthcheckTimestamp(workerID int) error {
	q := fmt.Sprintf("UPDATE %v SET healthchecked_at = '%v' WHERE id = %v", WorkerTable.Name(), time.Now().UTC().Format("2006-01-02 15:04:05"), workerID)
	// fmt.Println(">>>", q)
	if _, err := Repo.DB.Exec(q); err != nil {
		return err
	}
	return nil
}
