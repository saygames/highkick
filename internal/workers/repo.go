package workers

import (
	"fmt"

	"gopkg.in/reform.v1"

	repositoryLib "bitbucket.org/saygames/repository/pkg/mysql"
)

var Repo repositoryLib.Repository[Worker, *Worker]

func InitializeRepo(dbr *reform.DB) {
	Repo = repositoryLib.Repository[Worker, *Worker]{
		DB:        dbr,
		View:      WorkerTable,
		EnableLog: false,
	}
	fmt.Println("[Repo] [Workers] Initialized")
}
