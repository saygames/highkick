package workers

import (
	"fmt"
	"strings"
)

type QueryBuilder struct {
	ID      *int
	SID     *string
	Stopped *bool
	Page    *int
	PerPage *int
}

func (f QueryBuilder) Select() *[]string {
	return nil
}

func (f QueryBuilder) ForceIndex() *string {
	return nil
}

func (f QueryBuilder) Join() *string {
	return nil
}

func (f QueryBuilder) Where() string {
	clauses := []string{"true"}

	if f.ID != nil {
		clauses = append(clauses, fmt.Sprintf("(id = %v)", *f.ID))
	}
	if f.SID != nil {
		clauses = append(clauses, fmt.Sprintf("(sid = '%v')", *f.SID))
	}
	if f.Stopped != nil {
		switch *f.Stopped {
		case true:
			clauses = append(clauses, fmt.Sprintf("(stopped = 1)"))
		case false:
			clauses = append(clauses, fmt.Sprintf("(stopped = 0)"))
		}
	}

	return strings.Join(clauses, " AND ")
}

func (qb QueryBuilder) GroupBy() *[]string {
	return nil
}

func (qb QueryBuilder) OrderBy() *string {
	return nil
}

func (qb QueryBuilder) Pagination() *struct {
	Page    int
	PerPage int
} {
	if qb.Page != nil && qb.PerPage != nil {
		return &struct {
			Page    int
			PerPage int
		}{
			Page:    *qb.Page,
			PerPage: *qb.PerPage,
		}
	}
	return nil
}
